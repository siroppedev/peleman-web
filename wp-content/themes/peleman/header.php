<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700&display=swap" rel="stylesheet">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<header id="masthead" class="header">

		<nav class="nav-ppal">
			<div class="titles">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'menu_principal',
						'container' => false
					));
				?>
			</div>
			<div class="icon">
				<ul class="menu menu-search">
					<li>
						<a href=""><img src="<?php echo get_template_directory_uri(); ?>/img/search.svg" alt=""></a>
					</li>
				</ul>
				<?php
					wp_nav_menu( array(
						'theme_location' => 'menu_icons',
						'container' => false
					));
				?>
			</div>
		</nav>

		<div class="nav-account">
			<?php if ( is_user_logged_in() ) { ?>
				<ul class="nav-dropdown">
					<?php global $current_user;
						get_currentuserinfo();
					?>
					<?php if($current_user->display_name) { ?>
						<li>
							<a href="/mi-cuenta">
								<img src="<?php echo get_template_directory_uri() ?>/img/menu-usuario/004-user.svg" alt="">
								<p><?php echo $current_user->display_name; ?></p>
							</a>
						</li>
					<?php } ?>
					<?php wc_get_template('myaccount/navigation.php'); ?>
				</ul>
			<?php } else { ?>
				<div class="account-login-inner">
					<div class="account">
						<h3 class="uppercase"><?php esc_html_e( 'Login', 'woocommerce' ); ?></h3>
						<form class="woocommerce-form woocommerce-form-login login" method="post">

							<?php do_action( 'woocommerce_login_form_start' ); ?>

							<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
								<label for="username"><?php esc_html_e( 'Username or email address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
								<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
							</p>
							<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
								<label for="password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
								<input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" autocomplete="current-password" />
							</p>

							<?php do_action( 'woocommerce_login_form' ); ?>

							<p class="form-row">
								<label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
									<input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php esc_html_e( 'Remember me', 'woocommerce' ); ?></span>
								</label>
								<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
								<button type="submit" class="woocommerce-Button btn-secondary button woocommerce-form-login__submit" name="login" value="<?php esc_attr_e( 'Log in', 'woocommerce' ); ?>"><?php esc_html_e( 'Log in', 'woocommerce' ); ?></button>
							</p>
							<p class="woocommerce-LostPassword lost_password">
								<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'woocommerce' ); ?></a>
							</p>

							<?php do_action( 'woocommerce_login_form_end' ); ?>

						</form>
					</div>
					<div class="register">
						<h3 class="register-title">¿Todavía no tienes cuenta?</h3>
						<a class="btn-secondary" href="/mi-cuenta"><p>Regístrate</p></a>
					</div>
				</div>
			<?php } ?>
		</div>

		<nav class="nav-second">
			<?php
				wp_nav_menu( array(
					'theme_location' => 'menu_secundario',
					'container' => false
				));
			?>
			<div class="social-media">
				<a target="_blank" href="https://www.facebook.com/pages/category/Advertising-Marketing/Mr-Mrs-Peleman-103344134392542/"><i class="fab fa-facebook-f"></i></a>
				<!-- <a target="_blank" href=""><i class="fab fa-twitter"></i></a> -->
				<a target="_blank" href="https://www.instagram.com/mrmrspeleman/"><i class="fab fa-instagram"></i></a>
			</div>

			<div class="box-dots"></div>
			<div class="imagen-hover"></div>
		</nav>

		<nav class="nav-products main-wrapper-1440">
			<!-- <div class="main-wrapper"> -->
				<div class="content--nav-products">
					<div class="menu-image-products">
						<!-- <div class="image-products"></div> -->
						<?php
						
						$taxonomies = get_terms( array(
							'taxonomy' => 'product_cat',
							'hide_empty' => false,
							'parent' => 0
						));

						foreach($taxonomies as $parent_term ) { ?>
							<?php if($parent_term->slug != 'impresoras-y-packs' && $parent_term->slug != 'otros-productos') { ?>
								<?php $thumb_id = get_woocommerce_term_meta( $parent_term->term_id, 'thumbnail_id', true ) ?>
								<div class="image-products <?php echo $parent_term->slug ?>" style="background-image:url(<?php echo wp_get_attachment_url($thumb_id); ?>)"></div>
								<?php
								$taxonomies_child = get_terms( array(
									'taxonomy' => 'product_cat',
									'hide_empty' => false,
									'parent' => $parent_term->term_id
								)); 
								
								foreach($taxonomies_child as $child_term ) { ?>
									<?php $thumb_id = get_woocommerce_term_meta( $child_term->term_id, 'thumbnail_id', true ) ?>
									<div class="image-products <?php echo $child_term->slug ?>" style="background-image:url(<?php echo wp_get_attachment_url($thumb_id); ?>)"></div>
								<?php } ?>
							<?php } ?>
						<?php } ?>
					</div>
					<div class="menu--nav-products">
						<?php
						
						$taxonomies = get_terms( array(
							'taxonomy' => 'product_cat',
							'hide_empty' => false,
							'parent' => 0
						));

						foreach($taxonomies as $parent_term ) { ?>
							<?php if($parent_term->slug != 'impresoras-y-packs' && $parent_term->slug != 'otros-productos') { ?>
								<div class="list-category" data-rel="<?php echo $parent_term->slug ?>">
									<?php $thumb_id = get_woocommerce_term_meta( $parent_term->term_id, 'thumbnail_id', true ) ?>
									<a data-image="<?php echo $parent_term->slug; ?>" href="<?php echo get_category_link( $parent_term ); ?>">
										<img class="image_response" src=" <?php echo wp_get_attachment_url(  $thumb_id ); ?>" alt="">
										<h3><?php echo $parent_term->name; ?></h3>
									</a>
									<ul>
										<?php
										$taxonomies_child = get_terms( array(
											'taxonomy' => 'product_cat',
											'hide_empty' => false,
											'parent' => $parent_term->term_id
										)); 
										
										foreach($taxonomies_child as $child_term ) { ?>

											<?php $mypost_id = ''; ?>

											<?php if($child_term->count == 1) {
												$args = array( 
													'posts_per_page' => 1,
													'post_type' => 'product', 
													'tax_query' => array(
														array(
															'taxonomy' => 'product_cat',
															'field' => 'term_id',
															'terms' => $child_term->term_id
														)
													)
												);

												$myposts = get_posts( $args );
												$mypost_id = $myposts[0]->ID;
											} ?>
											<?php $thumb_id = get_woocommerce_term_meta( $child_term->term_id, 'thumbnail_id', true ) ?>
											<li><a data-image="<?php echo $child_term->slug; ?>" href="<?php echo ($mypost_id == '') ? get_category_link($child_term) : get_the_permalink($mypost_id); ?>">
												<img class="image_response" src=" <?php echo wp_get_attachment_url(  $thumb_id ); ?>" alt="">
												<p><?php echo $child_term->name; ?></p>
											</a></li>
										<?php } ?>
									</ul>
								</div>
							<?php } ?>
						<?php } ?>

						<a href="/todos-los-productos" class="view-all">Ver todos los productos</a>

					</div>
				</div>
			<!-- </div> -->
		</nav>

		<div class="nav-search">
			<input type="text" id="myInput" placeholder="Buscar producto...">
		</div>

		<div class="nav-image">
			<a href="/" class="contain-image">
				<img class="mobile" src="<?php echo get_template_directory_uri() ?>/img/logo-mobile.svg" alt="">
				<img class="desktop" src="<?php echo get_template_directory_uri() ?>/img/logo.svg" alt="">
			</a>
		</div>

		<div class="nav-btn">
			<a href="#" class="btn-menu">
				<span></span>
				<span></span>
				<span></span>
			</a>
		</div>

	</header>

	<div class="transparencia"></div>

    <div class="se-pre-con"></div>

