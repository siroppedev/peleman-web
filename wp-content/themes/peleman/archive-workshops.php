<?php 

/**
 * Template Name: Archive Workshops
 *
 */

get_header(); ?>

<?php $id_first = ''; ?>
<?php $cont_workshops = 0; ?>

<?php $todays_date = date("d-m-Y");
$today = strtotime($todays_date); ?>

<?php $args = array (
    'post_type' => 'workshops',
    'posts_per_page' => -1,
);
$query = new WP_Query($args);
if($query->have_posts()) : while($query->have_posts()) : $query->the_post();
    $cont_workshops++;
endwhile; endif; wp_reset_postdata(); ?>

<div class="cont-workshops" style="display:none"><?php echo $cont_workshops; ?></div>

    <div class="srp-mainrow workshops">

        <div class="workshop--main">
            <div class="main-wrapper-1440">
                <div class="workshop--main--content">
                    <?php $args = array (
                        'post_type' => 'workshops',
                        'posts_per_page' => 1,
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); 
                    
                    $id_first = get_the_ID(); 
                    
                    $exp_date = get_field('fecha_de_evento');
                    $exp_date = str_replace('/', '-', $exp_date); 
                    $expiration_date = strtotime($exp_date); ?>

                        <div class="item-workshops <?php echo ($expiration_date < $today)?'desactive':'';?>">
                            <div class="imagen" style="background-image:url(<?php echo get_the_post_thumbnail_url(); ?>)">
                                <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                            </div>
                            <div class="info">
                                <h4><?php echo get_the_title(); ?></h4>
                                <ul class="especificaciones">
                                    <?php foreach(get_field('especificaciones') as $esp) { ?>
                                        <li>
                                            <img src="<?php echo $esp['logo']; ?>" alt="">
                                            <p><?php echo $esp['texto']; ?></p>
                                        </li>
                                    <?php } ?>
                                </ul>
                                <div class="texto">
                                    <?php $text = apply_filters('the_content', get_post(get_the_ID())->post_content);
                                    echo $text; ?>
                                </div>
                                <?php if(is_null(get_field('lleno')) && get_field('lleno') == FALSE ) { ?>
                                    <a href="" class="btn-secondary"><p><?php echo pll__('Reservar', 'Peleman'); ?></p></a>
                                <?php } else { ?>
                                    <div class="sin-plaza">
                                        <p><?php echo pll__('Sin plaza', 'Peleman'); ?></p>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php endwhile; endif; wp_reset_postdata(); ?>
                </div>
            </div>
        </div>

        <div class="workshop--rest">
            <div class="main-wrapper">
                <div class="workshop--rest--content">
                    <?php $args = array (
                        'post_type' => 'workshops',
                        'posts_per_page' => 2,
                        'post__not_in' => array($id_first),
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()) : while($query->have_posts()) : $query->the_post();
                    
                    $exp_date = get_field('fecha_de_evento');
                    $exp_date = str_replace('/', '-', $exp_date); 
                    $expiration_date = strtotime($exp_date); ?>

                        <div class="item-workshops <?php echo ($expiration_date < $today)?'desactive':'';?>" >
                            <div class="imagen" style="background-image:url(<?php echo get_the_post_thumbnail_url(); ?>)">
                                <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                            </div>
                            <div class="info">
                                <h4><?php echo get_the_title(); ?></h4>
                                <ul class="especificaciones">
                                    <?php foreach(get_field('especificaciones') as $esp) { ?>
                                        <li>
                                            <img src="<?php echo $esp['logo']; ?>" alt="">
                                            <p><?php echo $esp['texto']; ?></p>
                                        </li>
                                    <?php } ?>
                                </ul>
                                <div class="texto">
                                    <?php $text = apply_filters('the_content', get_post(get_the_ID())->post_content);
                                    echo $text; ?>
                                </div>
                                <?php if(is_null(get_field('lleno')) && get_field('lleno') == FALSE ) { ?>
                                    <a href="" class="btn-secondary"><p><?php echo pll__('Reservar', 'Peleman'); ?></p></a>
                                <?php } else { ?>
                                    <div class="sin-plaza">
                                        <p><?php echo pll__('Sin plaza', 'Peleman'); ?></p>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php endwhile; endif; wp_reset_postdata(); ?>
                </div>
            </div>
        </div>

        <div class="show-more" id="show-more-workshops">
            <a href="" class="btn-primary"><p><?php echo pll__('Cargar más', 'Peleman'); ?></p></a>
        </div>

        <div class="form--workshop">
            <div class="cancell">
                <span></span><span></span>
            </div>
            <div class="response-mail">
                <div class="content-response">
                    <div class="response">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/iconos-form/response-star.svg" alt="">
                        <h2>¡Petición procesada!</h2>
                        <p>Tendrás noticias nuestras muy pronto. Cualquier duda, contacta con nosotros. </p>
                    </div>
                    <div class="info">
                        <div class="info-div">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/iconos-form/response-phone.svg" alt="">
                            <h5>Teléfono</h5>
                            <p>(+34) 961 609 052</p>
                        </div>
                        <div class="info-div">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/iconos-form/response-mail.svg" alt="">
                            <h5>Email</h5>
                            <p>hola@pelemanstore.com</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="imagen"></div>
            <div class="info-workshop">
                <div class="title">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/icon-workshop.svg" alt="">
                    <h2>Workshops</h2>
                </div>
                <div class="title-workshop">
                    <h3></h3>
                </div>
                <ul class="especificaciones">

                </ul>
            </div>
            <div class="form">
                <?php echo get_field('formulario_workshop'); ?>
            </div>
        </div>

    </div>


<?php get_footer();?>