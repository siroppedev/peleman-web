<?php
/**
 * The template for displaying the footer.
 *
 * @package flatsome
 */

global $flatsome_opt;
?>

</main><!-- #main -->

<footer id="footer" class="footer">

	<div class="main-wrapper">
		<div class="footer-content">
			<div class="footer-menus">
				<div class="row-top">
					<div class="menu-container">
						<h2>Empresa</h2>
						<?php
							wp_nav_menu( array(
								'theme_location' => 'menu_footer_empresa',
								'container' => false
							));
						?>
					</div>
					<div class="menu-container">
						<h2>Servicios profesionales</h2>
						<?php
							wp_nav_menu( array(
								'theme_location' => 'menu_footer_servicios',
								'container' => false
							));
						?>
					</div>
					<div class="menu-container">
						<h2>Ayuda y dudas</h2>
						<?php
							wp_nav_menu( array(
								'theme_location' => 'menu_footer_ayuda',
								'container' => false
							));
						?>
					</div>
					<div class="menu-container">
						<h2>Legal</h2>
						<?php
							wp_nav_menu( array(
								'theme_location' => 'menu_footer_legal',
								'container' => false
							));
						?>
					</div>
				</div>
				<div class="row-bottom">
					<div class="social-media">
						<a target="_blank" href="https://www.facebook.com/pages/category/Advertising-Marketing/Mr-Mrs-Peleman-103344134392542/"><i class="fab fa-facebook-f"></i></a>
						<!-- <a target="_blank" href=""><i class="fab fa-twitter"></i></a> -->
						<a target="_blank" href="https://www.instagram.com/mrmrspeleman/"><i class="fab fa-instagram"></i></a>
					</div>
				</div>
			</div>
			<div class="footer-newsletter">
				<?php
					dynamic_sidebar( 'sidebar-news' );
				?>
			</div>
			<div class="footer-mosaico"></div>
		</div>
	</div>

</footer><!-- .footer-wrapper -->

</div><!-- #wrapper -->

<?php wp_footer(); ?>

</body>
</html>
