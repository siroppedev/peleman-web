<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package flatsome
 */
?>

<?php /*

<div id="secondary" class="widget-area <?php ?>" role="complementary">
	<?php do_action( 'before_sidebar' ); ?>
	<?php if ( ! dynamic_sidebar( 'sidebar-cats' ) ) : ?>
	<?php endif; // end sidebar widget area ?>
</div><!-- #secondary -->

*/ ?>

<div class="sidebar-general">
	<div class="main-sidebar-general">
		<p><?php woocommerce_page_title(); ?></p>
		<a href="" class="arrow">
			<img src="<?php echo get_template_directory_uri() ?>/img/icons-categories/arrow.svg" alt="">
		</a>
	</div>
	<div class="sidebar--nav-products">
		<?php
		
		$taxonomies = get_terms( array(
			'taxonomy' => 'product_cat',
			'hide_empty' => false,
			'parent' => 0
		));

		foreach($taxonomies as $parent_term ) { ?>
			<?php /* if($parent_term->slug != 'otros-productos') { */ ?>
				<div class="list-category" data-rel="<?php echo $parent_term->slug ?>">
					<?php $thumb_id = get_woocommerce_term_meta( $parent_term->term_id, 'thumbnail_id', true ) ?>
					<div class="row-sidebar">
						<a data-image="<?php echo $parent_term->slug; ?>" href="<?php echo get_category_link( $parent_term ); ?>">
							<p><?php echo $parent_term->name; ?></p>
						</a>
						<?php $taxonomies_child = get_terms( array(
							'taxonomy' => 'product_cat',
							'hide_empty' => false,
							'parent' => $parent_term->term_id
						)); ?>
						<?php if(count($taxonomies_child) > 0) { ?>
							<div class="arrow"><img src="<?php echo get_template_directory_uri() ?>/img/icons-categories/arrow.svg" alt=""></div>
						<?php } ?>

					</div>
					<?php $taxonomies_child = get_terms( array(
						'taxonomy' => 'product_cat',
						'hide_empty' => false,
						'parent' => $parent_term->term_id
					)); ?>

					<?php if(count($taxonomies_child) > 0) { ?>
						<ul class="list-category--second">
							<?php foreach($taxonomies_child as $child_term ) { ?>

								<?php $mypost_id = ''; ?>

								<?php if($child_term->count == 1) {
									$args = array( 
										'posts_per_page' => 1,
										'post_type' => 'product', 
										'tax_query' => array(
											array(
												'taxonomy' => 'product_cat',
												'field' => 'term_id',
												'terms' => $child_term->term_id
											)
										)
									);

									$myposts = get_posts( $args );
									$mypost_id = $myposts[0]->ID;
								} ?>
								<?php $thumb_id = get_woocommerce_term_meta( $child_term->term_id, 'thumbnail_id', true ) ?>
								<li><a data-image="<?php echo $child_term->slug; ?>" href="<?php echo ($mypost_id == '') ? get_category_link($child_term) : get_the_permalink($mypost_id); ?>">
									<p><?php echo $child_term->name; ?></p>
								</a></li>
							<?php } ?>
						</ul>
					<?php } ?>
				</div>
			<?php /* } */ ?>
		<?php } ?>

	</div>
</div>
