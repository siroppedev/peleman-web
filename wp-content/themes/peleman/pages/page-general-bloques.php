<?php 

/**
 * Template Name: Page General bloques
 *
 */

get_header(); ?>

    <div class="srp-mainrow general-bloques">

        <?php if(get_field('barra_de_menu_general') == TRUE || is_null(get_field('barra_de_menu_general'))) { ?>
            <div class="srp-section bars-bloque">
                <!-- <div class="main-wrapper"> -->
                    <div class="bars-bloque--content">
                        <div class="bar--content-ppal">
                            <h3></h3>
                            <div class="arrow">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/arrow-blue.svg" alt="">
                            </div>
                        </div>
                        <ul class="bar--content-sec">
                            <?php $cont = 0; ?>
                            <?php foreach(get_field('bloques') as $bloque) {
                                if($bloque['barra_de_menu']) { 
                                    if ($bloque['nombre_barra_de_menu'] != '') { ?>
                                        <li>
                                            <a href="#section-<?php echo $cont; ?>" data-name="section-<?php echo $cont; ?>" class="menu-section menu-section-<?php echo $cont; ?>">
                                                <h4><?php echo $bloque['nombre_barra_de_menu'] ?></h4>
                                                <h3><?php echo $bloque['nombre_barra_de_menu'] ?></h3>
                                            </a>
                                        </li>
                                    <?php $cont++; }
                                }
                            } ?>
                        </ul>
                    </div>
                <!-- </div> -->
            </div>
        <?php } ?>

        <div class="srp-section general-bloque">

            <?php $contador = 0; ?>
            <?php foreach(get_field('bloques') as $bloque) { ?>

                <?php if($bloque['acf_fc_layout'] == 'imagen_izq_+_texto') { ?>
                    <div data-name="<?php echo $bloque['nombre_barra_de_menu'] ?>" id="<?php echo ($bloque['barra_de_menu'])?'section-' . $contador . '':'' ?>" class="<?php echo ($bloque['barra_de_menu'])?'section-bloque':'' ?> main-wrapper-right">
                        <div class="imagen-izq-texto--content">
                            <div class="imagen" style="background-image:url(<?php echo $bloque['imagen_iit']; ?>)"></div>
                            <div class="info">
                                <div class="box-text <?php echo $bloque['color_c_iit'] ?>">
                                    <h1><?php echo $bloque['titulo_iit'] ?></h1>
                                </div>
                                <div class="text-general-bloques">
                                    <?php echo $bloque['texto_iit'] ?>
                                </div>
                                <?php if($bloque['link_iit']) { ?>
                                    <a class="btn-primary" href="<?php echo $bloque['link_iit']; ?>">
                                        <p><?php echo $bloque['texto_link_iit']; ?></p>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php if($bloque['barra_de_menu']) { $contador ++; } ?>

                <?php } elseif($bloque['acf_fc_layout'] == 'dos_imagenes_superpuestas_+_texto') { ?>

                    <div data-name="<?php echo $bloque['nombre_barra_de_menu'] ?>" id="<?php echo ($bloque['barra_de_menu'])?'section-' . $contador . '':'' ?>" class="<?php echo ($bloque['barra_de_menu'])?'section-bloque':'' ?> main-wrapper">
                        <div class="dos-imagenes-superpuestas">
                            <div class="imagenes">
                                <div class="box-dots"></div>
                                <div class="imagen_sup" style="background-image:url(<?php echo $bloque['imagen_1_dist'] ?>)"></div>
                                <div class="imagen_inf" style="background-image:url(<?php echo $bloque['imagen_2_dist'] ?>)"></div>
                            </div>
                            <div class="info">
                                <div class="box-text <?php echo $bloque['color_c_dist'] ?>">
                                    <h1><?php echo $bloque['titulo_dist'] ?></h1>
                                </div>
                                <div class="text-general-bloques">
                                    <?php echo $bloque['texto_dist'] ?>
                                </div>
                                <?php if($bloque['link_dist']) { ?>
                                    <a class="btn-primary" href="<?php echo $bloque['link_dist']; ?>">
                                        <p><?php echo $bloque['nombre_link_dist']; ?></p>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php if($bloque['barra_de_menu']) { $contador ++; } ?>

                <?php } elseif($bloque['acf_fc_layout'] == 'imagen_+_texto_color_fondo') { ?>

                    <div data-name="<?php echo $bloque['nombre_barra_de_menu'] ?>" id="<?php echo ($bloque['barra_de_menu'])?'section-' . $contador . '':'' ?>" class="<?php echo ($bloque['barra_de_menu'])?'section-bloque':'' ?> main-wrapper-1340">
                        <div class="imagen-texto-color-fondo">
                            <div class="imagen" style="background-image:url(<?php echo $bloque['imagen_itcf']; ?>)"></div>
                            <div class="info" style="background-color: <?php echo $bloque['color_fondo_itcf']; ?>" >
                                <div class="box-text <?php echo $bloque['color_c_itcf'] ?>">
                                    <h1><?php echo $bloque['titulo_itcf'] ?></h1>
                                </div>
                                <div class="text-general-bloques">
                                    <?php echo $bloque['texto_itcf'] ?>                                
                                </div>
                                <a href="<?php echo $bloque['enlace_itcf'] ?>" class="btn-primary">
                                    <p>Descúbrela</p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php if($bloque['barra_de_menu']) { $contador ++; } ?>

                <?php } elseif($bloque['acf_fc_layout'] == 'galeria_imagenes_+_texto_con_fondo_parcial')  { ?>

                    <div data-name="<?php echo $bloque['nombre_barra_de_menu'] ?>" id="<?php echo ($bloque['barra_de_menu'])?'section-' . $contador . '':'' ?>" class="<?php echo ($bloque['barra_de_menu'])?'section-bloque':'' ?> galeria-imagenes-texto-fondo-parcial">
                        <div class="main-wrapper-left">
                            <div class="fondo" style="background-color: <?php echo $bloque['color_fondo_gitcfp'] ?>"></div>
                            <div class="section-home-products--content">
                                <div class="info">
                                    <div class="box-text <?php echo $bloque['color_c_gitcfp'] ?>">
                                        <h1><?php echo $bloque['titulo_gitcfp'] ?></h1>
                                    </div>
                                    <div class="text-secondary">
                                        <?php echo $bloque['texto_gitcfp'] ?>
                                    </div>
                                    <?php if($bloque['link_gitcfp']) { ?>
                                        <a class="btn-secondary" href="<?php echo $bloque['link_gitcfp']; ?>">
                                            <p><?php echo $bloque['texto_link_gitcfp']; ?></p>
                                        </a>
                                    <?php } ?>
                                </div>
                                <div class="carousel-home-products--content">
                                    <div class="carousel-home-products-block owl-carousel owl-theme">
                                        <?php foreach($bloque['galeria_gitcfp'] as $product) { ?>
                                            <div style="background-image:url(<?php echo $product; ?>)" class="item-product"></div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if($bloque['barra_de_menu']) { $contador ++; } ?>

                <?php } elseif($bloque['acf_fc_layout'] == 'titulo_+_iframe')  { ?>

                    <div data-name="<?php echo $bloque['nombre_barra_de_menu'] ?>" id="<?php echo ($bloque['barra_de_menu'])?'section-' . $contador . '':'' ?>" class="<?php echo ($bloque['barra_de_menu'])?'section-bloque':'' ?> main-wrapper">
                        <div class="titulo-iframe">
                            <h1><?php echo $bloque['titulo_ti']; ?></h1>
                            <div class="iframe">
                                <iframe src="<?php echo $bloque['iframe_ti']; ?>" height="600" frameborder="0" style="border:0" allowfullscreen=""></iframe>
                            </div>
                            <a class="btn-primary" href="/contacto"><p>¡Ven a conocernos!</p></a>
                        </div>
                    </div>
                    <?php if($bloque['barra_de_menu']) { $contador ++; } ?>

                <?php } elseif($bloque['acf_fc_layout'] == 'imagen_vertical_+_texto') { ?>

                    <div class="pop-up--solicitud">
                        <div class="cancell">
                            <span></span><span></span>
                        </div>
                        <div class="response-mail">
                            <div class="content-response">
                                <div class="response">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/iconos-form/response-star.svg" alt="">
                                    <h2>¡Petición procesada!</h2>
                                    <p>Tendrás noticias nuestras muy pronto. Cualquier duda, contacta con nosotros. </p>
                                </div>
                                <div class="info">
                                    <div class="info-div">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/iconos-form/response-phone.svg" alt="">
                                        <h5>Teléfono</h5>
                                        <p>(+34) 961 609 052</p>
                                    </div>
                                    <div class="info-div">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/iconos-form/response-mail.svg" alt="">
                                        <h5>Email</h5>
                                        <p>hola@pelemanstore.com</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="imagen" style="background-image: url(<?php echo $bloque['imagen_pop_up_ivt']; ?>)"></div>
                        <div class="title">
                            <div class="imagen-title"></div>
                            <h2>Solicitar sala</h2>
                        </div>
                        <div class="form">
                            <?php echo $bloque['formulario_ivt']; ?>
                        </div>
                    </div>
                    <div data-name="<?php echo $bloque['nombre_barra_de_menu'] ?>" id="<?php echo ($bloque['barra_de_menu'])?'section-' . $contador . '':'' ?>" class="<?php echo ($bloque['barra_de_menu'])?'section-bloque':'' ?> main-wrapper">
                        <div class="imagen-vertical-texto">
                            <div class="imagen" style="background-image: url(<?php echo $bloque['imagen_ivt']; ?>)"></div>
                            <div class="info">
                                <div class="box-text <?php echo $bloque['color_c_ivt'] ?>">
                                    <h1><?php echo $bloque['titulo_ivt'] ?></h1>
                                </div>
                                <div class="text-general-bloques">
                                    <?php echo $bloque['texto_ivt'] ?>
                                </div>
                                <?php if($bloque['nombre_link_ivt']) { ?>
                                    <a class="btn-primary" href="">
                                        <p><?php echo $bloque['nombre_link_ivt']; ?></p>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php if($bloque['barra_de_menu']) { $contador ++; } ?>

                <?php } elseif($bloque['acf_fc_layout'] == 'dos_imagenes_+_texto') { ?>

                    <div data-name="<?php echo $bloque['nombre_barra_de_menu'] ?>" id="<?php echo ($bloque['barra_de_menu'])?'section-' . $contador . '':'' ?>" class="<?php echo ($bloque['barra_de_menu'])?'section-bloque':'' ?> main-wrapper-right">
                        <div class="dos-imagenes-texto">
                            <div class="imagenes">
                                <div class="imagen_sup" style="background-image: url(<?php echo $bloque['imagen_1_dit']; ?>)"></div>
                                <div class="box-dots"></div>
                                <div class="imagen_inf" style="background-image: url(<?php echo $bloque['imagen_2_dit']; ?>)"></div>
                            </div>
                            <div class="info-content-sticky">
                                <div class="info">
                                    <div class="box-text <?php echo $bloque['color_c_dit'] ?>">
                                        <h1><?php echo $bloque['titulo_dit'] ?></h1>
                                    </div>
                                    <div class="text-general-bloques">
                                        <?php echo $bloque['texto_dit'] ?>
                                    </div>
                                    <?php if($bloque['link_dit']) { ?>
                                        <a class="btn-primary" href="<?php echo $bloque['link_dit']; ?>">
                                            <p><?php echo $bloque['nombre_link_dit']; ?></p>
                                        </a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if($bloque['barra_de_menu']) { $contador ++; } ?>

                <?php } elseif($bloque['acf_fc_layout'] == 'texto_gigante_+_imagen_y_texto') { ?>

                    <div data-name="<?php echo $bloque['nombre_barra_de_menu'] ?>" id="<?php echo ($bloque['barra_de_menu'])?'section-' . $contador . '':'' ?>" class="<?php echo ($bloque['barra_de_menu'])?'section-bloque':'' ?> main-wrapper-1000">
                        <div class="texto-gigante-con-imagen">
                            <div class="texto-gigante"><h1><?php echo $bloque['texto_gigante_tgit'] ?></h1></div>
                            <div class="imagen" style="background-image:url(<?php echo $bloque['imagen_tgit']; ?>)"></div>
                            <div class="info">
                                <div class="text-general-bloques">
                                    <?php echo $bloque['texto_tgit'] ?>
                                </div>
                                <?php if($bloque['link_tgit']) { ?>
                                    <a class="btn-primary" href="<?php echo $bloque['link_tgit']; ?>">
                                        <p><?php echo $bloque['texto_link_tgit']; ?></p>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php if($bloque['barra_de_menu']) { $contador ++; } ?>

                <?php } elseif($bloque['acf_fc_layout'] == 'cuatro_bloques_contacto') { ?>

                    <?php get_template_part( 'template-parts/template', 'contacto' ); ?>                    
                    <?php if($bloque['barra_de_menu']) { $contador ++; } ?>

                <?php } elseif($bloque['acf_fc_layout'] == 'productos_destacados') { ?>

                    <div class="srp-section section-home-products">
                        <div class="main-wrapper-right">
                            <div class="fondo"></div>
                            <div class="section-home-products--content">
                                <div class="box-text">
                                    <h1><?php echo $bloque['titulo_pd'] ?></h1>
                                </div>
                                <div class="text-secondary">
                                    <h3><?php echo $bloque['subtitulo_pd']; ?></h3>
                                </div>
                                <div class="carousel-home-products--content">
                                    <div class="box-dots"></div>
                                    <div class="carousel-home-products owl-carousel owl-theme">
                                        <?php foreach($bloque['productos_destacados_pd'] as $product) { ?>
                                            <a href="<?php echo get_the_permalink($product); ?>" style="background-image:url(<?php echo get_the_post_thumbnail_url($product); ?>)" class="item-product">
                                                <div class="opacity">
                                                    <h3><?php echo get_the_title($product); ?></h3>
                                                </div>
                                            </a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if($bloque['barra_de_menu']) { $contador ++; } ?>

                <?php } elseif($bloque['acf_fc_layout'] == 'formulario_de_contacto') { ?>

                    <div class="main-wrapper-600">
                        <div class="formulario-contacto--content">
                            <div class="box-dots"></div>
                            <div class="form">
                                <?php echo $bloque['formulario_fdc']; ?>
                            </div>
                        </div>
                    </div>
                    <?php if($bloque['barra_de_menu']) { $contador ++; } ?>

                <?php } elseif($bloque['acf_fc_layout'] == 'galeria_imagenes_cuadricula') { ?>

                    <div class="main-wrapper-1000">
                        <div class="galeria-cuadricula--content">
                            <div class="info">
                                <div class="box-text <?php echo $bloque['color_c_gic'] ?>">
                                    <h1><?php echo $bloque['titulo_gic'] ?></h1>
                                </div>
                            </div>
                            <div class="carousel-cuadricula mobile owl-carousel owl-theme">
                                <?php foreach($bloque['contenido_gic'] as $content) { ?>
                                    <div class="item">
                                        <div class="imagen" style="background-image:url(<?php echo $content['imagen_gic']; ?>)"></div>
                                        <div class="text-general-bloques">
                                            <?php echo $content['texto_repeater_gic']; ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="carousel-cuadricula desktop">
                                <?php foreach($bloque['contenido_gic'] as $content) { ?>
                                    <div class="item">
                                        <div class="imagen" style="background-image:url(<?php echo $content['imagen_gic']; ?>)"></div>
                                        <div class="text-general-bloques">
                                            <?php echo $content['texto_repeater_gic']; ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <?php ?>
                            <?php if($bloque['link_gic']) { ?>
                                <a class="btn-primary" href="<?php echo $bloque['link_gic']; ?>">
                                    <p><?php echo $bloque['texto_link_gic']; ?></p>
                                </a>
                            <?php } ?>
                        </div>
                    </div>

                <?php } elseif($bloque['acf_fc_layout'] == 'galeria_productos_cuadricula') { ?>

                    <div class="main-wrapper-1000">
                        <div class="galeria-cuadricula--content">
                            <div class="info">
                                <div class="box-text <?php echo $bloque['color_c_gpc'] ?>">
                                    <h1><?php echo $bloque['titulo_gpc'] ?></h1>
                                </div>
                            </div>
                            <div class="carousel-cuadricula mobile owl-carousel owl-theme">
                                <?php foreach($bloque['contenido_gpc'] as $content) { ?>
                                    <div class="item">
                                        <div class="imagen" style="background-image:url(<?php echo $content['imagen_gpc']; ?>)"></div>
                                        <div class="text-general-bloques">
                                            <?php echo $content['texto_repeater_gpc']; ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="carousel-cuadricula desktop">
                                <?php foreach($bloque['contenido_gpc'] as $content) { ?>
                                    <a href="<?php echo get_the_permalink($content); ?>" class="item">
                                        <div class="imagen" style="background-image:url(<?php echo get_the_post_thumbnail_url($content); ?>)"></div>
                                        <div class="text-general-bloques">
                                            <h3><?php echo get_the_title($content); ?></h3>
                                        </div>
                                    </a>
                                <?php } ?>
                            </div>
                            <?php if($bloque['link_gpc']) { ?>
                                <a class="btn-primary" href="<?php echo $bloque['link_gpc']; ?>">
                                    <p><?php echo $bloque['texto_link_gpc']; ?></p>
                                </a>
                            <?php } ?>
                        </div>
                    </div>

                <?php } elseif ($bloque['acf_fc_layout'] == 'galeria_imagenes_cuadricula_cuatro') { ?>

                    <div class="main-wrapper-1340">
                        <div class="galeria-cuadricula-cuatro--content">
                            <div class="info">
                                <div class="box-text <?php echo $bloque['color_c_gicc'] ?>">
                                    <h1><?php echo $bloque['titulo_gicc'] ?></h1>
                                </div>
                            </div>
                            <div class="carousel-cuadricula_cuatro mobile owl-carousel owl-theme">
                                <?php foreach($bloque['contenido_gicc'] as $content) { ?>
                                    <div class="item">
                                        <div class="imagen" style="background-image:url(<?php echo $content['imagen_gicc']; ?>)"></div>
                                        <div class="text-general-bloques">
                                            <?php echo $content['texto_repeater_gicc']; ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="carousel-cuadricula_cuatro desktop">
                                <?php foreach($bloque['contenido_gicc'] as $content) { ?>
                                    <div class="item">
                                        <div class="imagen" style="background-image:url(<?php echo $content['imagen_gicc']; ?>)"></div>
                                        <div class="text-general-bloques">
                                            <?php echo $content['texto_repeater_gicc']; ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <?php if($bloque['link_gic']) { ?>
                                <a class="btn-primary" href="<?php echo $bloque['link_gic']; ?>">
                                    <p><?php echo $bloque['texto_link_gic']; ?></p>
                                </a>
                            <?php } ?>
                        </div>
                    </div>

                <?php } elseif ($bloque['acf_fc_layout'] == 'carosuel_productos_full_width') { ?>

                    <div class="carousel-productos-full-width owl-carousel owl-theme">
                        <?php foreach($bloque['productos'] as $products) { ?>
                            <a href="<?php echo get_the_permalink($products); ?>" class="item-prod">
                                <img class="desktop" src="<?php echo get_field('imagen_desktop',$products); ?>" alt="">
                                <img class="mobile" src="<?php echo get_field('imagen_mobile',$products); ?>" alt="">
                            </a>
                        <?php } ?>
                    </div>
                    
                <?php } elseif ($bloque['acf_fc_layout'] == 'carousel_como_testimonios') { ?>

                    <div data-name="<?php echo $bloque['nombre_barra_de_menu'] ?>" id="<?php echo ($bloque['barra_de_menu'])?'section-' . $contador . '':'' ?>" class="<?php echo ($bloque['barra_de_menu'])?'section-bloque':'' ?>">
                        <?php if(is_null($bloque['mostrar_bloque_cct']) || $bloque['mostrar_bloque_cct']) { ?>
                            <?php set_query_var( 'title', $bloque['titulo_cct']); get_template_part('templates/template', 'testimonios') ?>
                            <?php get_template_part( 'template-parts/template', 'testimonios' ); ?>
                        <?php } ?>
                    </div>
                    <?php if($bloque['barra_de_menu']) { $contador ++; } ?>

                <?php } ?>

            <?php } ?>

        </div>
    </div>


<?php get_footer();?>