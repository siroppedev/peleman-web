<?php 

/**
 * Template Name: Page Repeater like faqs
 *
 */

get_header(); ?>

    <div class="srp-mainrow faqs">

        <div class="srp-section section-faqs">
            <div class="main-wrapper main-wrapper-910">
                <div class="section-faqs--content">
                    <div class="box-text box-text-yellow">
                        <h1><?php echo get_the_title(); ?></h1>
                    </div>
                    <div class="faqs-repeater">
                        <?php foreach(get_field('preguntas_frecuentes') as $preguntas) { ?>
                            <div class="frequence-faq">
                                <div class="title">
                                    <h3><?php echo $preguntas['pregunta']; ?></h3>
                                    <div class="arrow"><img src="<?php echo get_template_directory_uri(); ?>/img/arrow-blue.svg" alt=""></div>
                                </div>
                                <div class="editor-wysiwyg">
                                    <?php echo $preguntas['respuesta']; ?>
                                    </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

    </div>


<?php get_footer();?>