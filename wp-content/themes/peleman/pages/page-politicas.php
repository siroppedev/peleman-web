<?php 

/**
 * Template Name: Page like politicas
 *
 */

get_header(); ?>

    <div class="srp-mainrow politicas">

        <div class="srp-section section-politicas">
            <div class="main-wrapper main-wrapper-910">
                <div class="section-politicas--content">
                    <div class="box-politicas">
                        <h3><?php echo get_the_title(); ?></h3>
                        <?php $text = apply_filters('the_content', get_post(get_the_ID())->post_content); ?>
                        <div class="editor-wysiwyg"><?php echo $text; ?></div>
                    </div>
                </div>
            </div>
        </div>

    </div>


<?php get_footer();?>