<?php 

/**
 * Template Name: Page Historia
 *
 */

get_header(); ?>

<div class="srp-mainrow history">

    <div class="srp-section section-history">
        <!-- <div class="main-wrapper"> -->
            <div class="section-history--content">
                <div class="box-dots"></div>
                <div class="carousel-history owl-carousel owl-theme">
                    <?php foreach(get_field('historia') as $history) { ?>
                        <div class="item-history" data-color="<?php echo $history['color_fondo'] ?>">
                            <div class="imagen" style="background-image: url(<?php echo $history['imagen']; ?>)">
                                <div class="texto-gigante"><h1><?php echo $history['ano'] ?></h1></div>
                            </div>
                            <div class="texto">
                                <?php echo $history['texto_historia']; ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <!-- </div> -->
    </div>

</div>


<script type="text/javascript" src="<?=get_template_directory_uri()?>/js/jquery.scrollbar.js"></script>
<script type="text/javascript">
    jQuery('.scrollbar-inner').scrollbar({
        "showArrows": true,
        "scrollx": "advanced",
        "scrolly": "advanced"
    });
</script>

<?php get_footer();?>