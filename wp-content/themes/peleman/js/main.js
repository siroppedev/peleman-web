$ = jQuery.noConflict();

$(window).load(function() {
    if($('.srp-mainrow.history').length > 0) {
        var color_inicial = $('.section-history--content .carousel-history .owl-item.active .item-history').attr('data-color');
        $('.srp-mainrow.history').css('background-color', color_inicial);
    }

    $(".se-pre-con").fadeOut("slow");
});


$(document).ready(function() { 

    //ScrollMagic
    var controller = new ScrollMagic.Controller();

    // ============================================================================== //
    // ========================        Header styles        ========================= //
    // ============================================================================== //


    // Animación Buscador

    var click_search = true;

    $('.menu-search li a').on('click', function(e) {
        e.preventDefault();

        var elem = $('a.btn-menu');
        var time = 300;
        var time_false = 1200;

        if(click_search) {

            if(elem.hasClass('active')) {

                click_search = false;
                setTimeout(function() {
                    click_search = true;
                }, time_false);

                elem.removeClass('third-move');
                setTimeout(function() {
                    elem.removeClass('second-move');
                }, time);
                setTimeout(function() {
                    elem.removeClass('first-move');
                }, time + 500);
                elem.removeClass('active');

                $('.nav-search').removeClass('active');
                $('.transparencia').removeClass('active-menu');
                $('#myInput').val('');

                if(window.innerWidth < 1200) {
                    $('body, html').css('overflow', 'auto');
                }
                
                if(window.innerWidth >= 992) {
                    setTimeout(function() {
                        $('.titles').removeClass('active');
                    }, 800);
                }

            } else {
                
                click_search = false;
                setTimeout(function() {
                    click_search = true;
                }, time_false);

                elem.addClass('first-move');
                setTimeout(function() {
                    elem.addClass('second-move');
                }, time);
                setTimeout(function() {
                    elem.addClass('third-move');
                }, time + 500);
                elem.addClass('active');

                $('.titles').addClass('active');
                
                if(window.innerWidth < 1200) {
                    $('body, html').css('overflow', 'hidden');
                }
                
                if(window.innerWidth < 992) {
                    $('.nav-search').addClass('active');
                    $('.titles').addClass('active');                    
                    $('.transparencia').addClass('active-menu');
                } else {
                    setTimeout(function() {
                        $('.nav-search').addClass('active');
                    }, 800);
                }

            }
        }
    
    
    });

    var arrayProductosVar = arrayProducts();

    autocomplete(document.getElementById("myInput"), arrayProductosVar);


    // Menu productos - Hover

    $('nav.nav-products .menu-image-products .image-products:nth-of-type(1)').addClass('active');

    $('nav.nav-products .menu--nav-products .list-category a').on('mouseenter', function() {
        $('nav.nav-products .menu-image-products .image-products').siblings().removeClass('active');
        $('nav.nav-products .menu-image-products .image-products.' + $(this).attr('data-image')).addClass('active');
        $(this).attr('data-image');
    });


    // Animación de hamburguesa X del menú

    var click = true;

    $('header.header .nav-btn .btn-menu').on('click', function(e) {
        e.preventDefault();
        var elem = $(this);
        var time = 300;
        var time_false = 1200;

        if(click) {

            if(elem.hasClass('active')) {

                click = false;
                $('body, html').css('overflow', 'auto');

                // Contenido que aparece del menu secundario
                $('.nav-second .social-media').removeClass('active');
                $('.nav-second .menu').removeClass('active');
                $('.nav-second .box-dots').removeClass('active');
                $('.nav-second .imagen-hover').removeClass('active');

                setTimeout(function() {
                    click = true;
                }, time_false);

                $('.nav-second').removeClass('active');
                $('.nav-search').removeClass('active');
                $('.transparencia').removeClass('active-menu');
                $('#myInput').val('');

                setTimeout(function() {
                    $('.titles').removeClass('active');
                }, 800);

                elem.removeClass('third-move');
                setTimeout(function() {
                    elem.removeClass('second-move');
                }, time);
                setTimeout(function() {
                    elem.removeClass('first-move');
                }, time + 500);
                elem.removeClass('active');
            } else {
                
                $('.nav-account').removeClass('active');
                $('nav.nav-products').removeClass('active');
                $('body, html').css('overflow', 'hidden');

                click = false;

                // Contenido que aparece del menu secundario
                setTimeout(function() {
                    $('.nav-second .social-media').addClass('active');
                    $('.nav-second .menu').addClass('active');
                    $('.nav-second .box-dots').addClass('active');
                    $('.nav-second .imagen-hover').addClass('active');
                }, time_false - 300);

                setTimeout(function() {
                    click = true;
                }, time_false);

                $('.nav-second').addClass('active');

                elem.addClass('first-move');
                setTimeout(function() {
                    elem.addClass('second-move');
                }, time);
                setTimeout(function() {
                    elem.addClass('third-move');
                }, time + 500);
                elem.addClass('active');
            }

        }

        // Final Animación de hamburguesa X del menú ========


    });

    // Hover menu secundario - Activacion de imagenes

    $('.nav-second .menu li').each(function(index, element) {
        var image_menu_second = $(this).find('img').attr('src');
        $('.nav-second .imagen-hover').append('<div style="background-image:url('+ image_menu_second +')"></div>');
        return index < 2; 
    });

    $('.nav-second .menu li').on('mouseenter', function() {
        var index_menu = $(this).index();
        $('.nav-second .imagen-hover div').siblings().removeClass('active');
        $($('.nav-second .imagen-hover div')[index_menu]).addClass('active');
        $('.nav-second .imagen-hover').addClass('hover');
    });

    $('.nav-second .menu li').on('mouseleave', function() {
        $('.nav-second .imagen-hover div').siblings().removeClass('active');
        $('.nav-second .imagen-hover').removeClass('hover');
    });

    // Activar menu Productos

    var userHoverTimeout;

    $('#menu-menu-principal li:nth-of-type(1) a').on('mouseenter', function() {
        userHoverTimeout = setTimeout(function () {
            $('nav.nav-products').addClass('active');
            $('.nav-account').removeClass('active');
        }, 180);
    });

    $('#menu-menu-principal li:nth-of-type(1) a').on('mouseleave', function() {
        clearTimeout(userHoverTimeout);
    });

    $('nav.nav-products').on('mouseleave', function() {
        $('nav.nav-products').removeClass('active');
        clearTimeout(userHoverTimeout);
    });

    // Activar menu Usuario

    // if(window.innerWidth < 1200) {
        $('#menu-menu-iconos li.menu-item:nth-of-type(1)').on('click', function(e) {
            e.preventDefault();
            $('nav.nav-products').removeClass('active');            
            if ($('.nav-account').hasClass('active')) {
                $('.nav-account').removeClass('active');
            } else {
                $('.nav-account').addClass('active');
            }
        });
    // } else {
    //     $('#menu-menu-iconos li.menu-item:nth-of-type(1)').on('click', function(e) {
    //         e.preventDefault();
    //     });
    //     $('#menu-menu-iconos li.menu-item:nth-of-type(1)').on('mouseenter', function() {
    //         $('.nav-account').addClass('active');
    //     });
    //     $('.nav-account').on('mouseleave', function() {
    //         $('.nav-account').removeClass('active');
    //     });
    // }


    // ============================================================================== //
    // ========================        Footer styles        ========================= //
    // ============================================================================== //

    var click_news = true;

    $('.form-group-news').on('click', function() {
        if(click_news) {
            $(this).find('.span').toggleClass('active');
            // console.log('cambio');
            click_news = false;
            setTimeout(function() {
                click_news = true;
            }, 300);
        }
    });

    $('.form-group-news a').on('click', function() {
        setTimeout(() => {
            if($(this).parent().parent().find('.span').hasClass('active')) {
                $(this).parent().parent().find('.span').removeClass('active');
                // console.log('remove');
            } else {
                $(this).parent().parent().find('.span').addClass('active');
                // console.log('add');
            }
        }, 400);
    });



    if($('.single-product--main').length) {
        $('#main.site-main').addClass('without-padding');
        if(window.innerWidth < 1200) {
            var altura_imagenes = $('#wooswipe').outerHeight();
            $('.fondo-prod').css('height', altura_imagenes + 160);
        }

        $('table.variations tr').each(function(index, element) {
            $(element).find('select').select2();
        });
    }


    // ============================================================================== //
    // ==========================        Home styles        ========================= //
    // ============================================================================== //

    $('.carousel-valors-home-mobile').owlCarousel({
        loop:true,
        nav:false,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:2
            },
            992: {
                items: 4,
                touchDrag: false,
                mouseDrag: false
            }
        }
    });

    $('.carousel-cuadricula.mobile').owlCarousel({
        loop:true,
        nav:false,
        responsive:{
            0:{
                margin: 20,
                items:1
            },
            768:{
                margin: 20,
                items:2
            },
            992: {
                margin: 20,
                items: 3
            }
        }
    });

    $('.carousel-cuadricula_cuatro.mobile').owlCarousel({
        loop:true,
        nav:false,
        responsive:{
            0:{
                margin: 20,
                items:1
            },
            768:{
                margin: 20,
                items:2
            },
            992: {
                margin: 20,
                items: 3
            }
        }
    });

    $('.carousel-single-products').owlCarousel({
        loop:true,
        nav:true,
        dots:false,
        margin: 20,
        navText: ["<img src='/wp-content/themes/peleman/img/angle-left.svg'>","<img src='/wp-content/themes/peleman/img/angle-right.svg'>"],
        responsive:{
            0:{
                items:1
            },
            768: {
                items:3,
                margin:0
            }
        }
    });

    $('.carousel-home-products').owlCarousel({
        loop:true,
        nav:true,
        dots:false,
        margin:20,
        navText: ["<img src='/wp-content/themes/peleman/img/angle-left.svg'>","<img src='/wp-content/themes/peleman/img/angle-right.svg'>"],
        responsive:{
            0:{
                items:1
            },
            768: {
                items:3,
                margin:0
            }
        }
    });

    $('.carousel-home-products-block').owlCarousel({
        loop:true,
        nav:true,
        dots:false,
        margin:20,
        navText: ["<img src='/wp-content/themes/peleman/img/angle-left.svg'>","<img src='/wp-content/themes/peleman/img/angle-right.svg'>"],
        responsive:{
            0:{
                items:1
            },
            768: {
                items:3,
            },
            992: {
                items: 2,
                stagePadding: 50,
            },
            1200: {
                items: 2,
                margin: 40,
                stagePadding: 50,
            }
        }
    });

    $('.carousel-productos-full-width').owlCarousel({
        loop:true,
        nav:true,
        dots:false,
        margin:0,
        navText: ["<img src='/wp-content/themes/peleman/img/angle-left.svg'>","<img src='/wp-content/themes/peleman/img/angle-right.svg'>"],
        responsive:{
            0:{
                items:1
            }
        }
    });


    $('.testimonios-carousel').owlCarousel({
        loop:true,
        dots:false,
        nav:true,
        navText: ["<img src='/wp-content/themes/peleman/img/angle-left.svg'>","<img src='/wp-content/themes/peleman/img/angle-right.svg'>"],
        responsive:{
            0:{
                margin: 20,
                items:1
            },
            1330:{
                margin: 50,
                items:2,
            }
        }
    });
    

    // ============================================================================== //
    // ==========================        FAQS styles        ========================= //
    // ============================================================================== //

    $('.faqs-repeater .frequence-faq').on('click', function(e) {
        e.preventDefault();
        if($(this).find('.arrow').hasClass('active')) {
            $(this).find('.editor-wysiwyg').slideUp();
            $(this).find('.arrow').removeClass('active');
        } else {
            $(this).find('.editor-wysiwyg').slideDown();
            $(this).find('.arrow').addClass('active');
        }
    });

    // ============================================================================== //
    // =======================        GENERAL styles        ========================= //
    // ============================================================================== //
    

    var total_carousel_products = $('.srp-section.section-home-products').length;

    $('.srp-section.section-home-products').each(function(index, element) {
        
        if(index + 1 != total_carousel_products) {
            $(element).addClass('duplicate');
        }
    });

    // Barra de navegación
    
    if($('.srp-section.bars-bloque').length > 0) {
        $('.bars-bloque--content .bar--content-ppal h3').html($('.bar--content-sec li:nth-of-type(1) a h4').text());
        $('.srp-mainrow.general-bloques').addClass('bar-class');
    }

    $('.srp-section.bars-bloque .bar--content-sec li a').on('click', function(e) {
        e.preventDefault();
        $('.bars-bloque--content .bar--content-ppal h3').html($(this).find('h4').text());
        $('.srp-section.bars-bloque .arrow').removeClass('active');
        if(window.innerWidth < 992) {
            $(this).parent().parent().slideUp();
        }

        var sectionID = $(this).attr("data-name");

        if(window.innerWidth < 1600) {        
            if($('#'+sectionID).position().top > window.pageYOffset) {
                $('html, body').animate({scrollTop: $("#"+sectionID).offset().top - 68 }, 1500);
            } else if($('#'+sectionID).position().top < window.pageYOffset) {
                $('html, body').animate({scrollTop: $("#"+sectionID).offset().top - 148 }, 1500);
            }
        } else {
            if($('#'+sectionID).position().top > window.pageYOffset) {
                $('html, body').animate({scrollTop: $("#"+sectionID).offset().top - 98 }, 1500);
            } else if($('#'+sectionID).position().top < window.pageYOffset) {
                $('html, body').animate({scrollTop: $("#"+sectionID).offset().top - 178 }, 1500);
            }
        }
        

    });

    // Esconder barra de navegacion cuando scroll down

    if($('.srp-section.bars-bloque').length > 0) {
        var i1 = 0; 
        var i2 = 0; 

        var scene = new ScrollMagic.Scene({
            triggerHook: 0,
        })
        .addTo(controller)
        .on("update", function() {
            var x1 = controller.info("scrollDirection");
            var x2 = $(window).scrollTop();
            var x3 = 0;
                if ( x1 == "REVERSE" && x2 >= x3 && i1 == 0) {
                    i1++;
                    i2 = 0;
                    $('.srp-section.bars-bloque').removeClass('hide');
                    $('header.header').removeClass('hide');
                }
                if ( x1 == "FORWARD" && x2 > x3 && i2 == 0) {
                    i1 = 0;
                    i2++;
                    $('.srp-section.bars-bloque').addClass('hide');
                    $('header.header').addClass('hide');
                }       
        });
    }

    // Scrollmagic para cambiar de nombre en la barra de navegación secundaria

    $(".section-bloque").each(function(i) {
        
        var breakID = $(this).attr('id');
        var nameSection = $(this).attr('data-name');
        var nameSectionPrev = $(this).prev().attr('data-name');
        // build scenes
        new ScrollMagic.Scene({
            triggerElement: this,
            offset: -150,
            triggerHook: 0,
        })
            //.setClassToggle('.menu-'+ breakID +'', 'active') // add class toggle
            .on('enter', function() {
                $('.menu-section').removeClass('active');
                $('.menu-'+ breakID +'').addClass('active');
                $('.bars-bloque--content .bar--content-ppal h3').html(nameSection);
            })
            .on('leave', function() {

                var pos = breakID.lastIndexOf('-');
                var prevBlock = $(this);
                var numID = breakID.substring(pos + 1);
                if(numID > 0) {    
                    var prevBreakID = breakID.substring(0, pos + 1) + (numID - 1);

                    $('.menu-section').removeClass('active');
                    $('.menu-'+ prevBreakID +'').addClass('active');
                    $('.bars-bloque--content .bar--content-ppal h3').html(nameSectionPrev);

                }
            })
            // .addIndicators()
            .addTo(controller);
        
    });

    // Boton para abrir - cerrar la barra de navegación secundaria de menú

    $('.srp-section.bars-bloque .arrow').on('click', function(e) {
        e.preventDefault();
        if($(this).hasClass('active')) {
            $(this).parent().parent().find('.bar--content-sec').slideUp();
            $(this).removeClass('active');
        } else {
            $(this).parent().parent().find('.bar--content-sec').slideDown();
            $(this).addClass('active');
        }
    });

    // Pop Up - Transparencia trasera que podrá cerrar la ventana

    $('body').on('click', '.transparencia', function(e) {
        e.preventDefault();
        $(this).removeClass('active-menu');
        $(this).removeClass('active');
        $('.nav-search').removeClass('active');
        $('.pop-up--solicitud').removeClass('active');

        $('.form--workshop').removeClass('active');
        $('.form--workshop').find('.info-workshop ul.especificaciones').empty();

        $('#myInput').val('');
        $('body, html').css('overflow', 'auto');

        var elem = $('a.btn-menu');
        var time = 300;
        var time_false = 1200;

        if(click_search) {

            if(elem.hasClass('active')) {

                click_search = false;
                setTimeout(function() {
                    click_search = true;
                }, time_false);

                elem.removeClass('third-move');
                setTimeout(function() {
                    elem.removeClass('second-move');
                }, time);
                setTimeout(function() {
                    elem.removeClass('first-move');
                }, time + 500);
                elem.removeClass('active');
            }
        }
        // $('body, html').css('overflow', 'auto');
    });

    // Una vez enviado el formulario

    $('.pop-up--solicitud .cancell').on('click', function() {
        $(this).parent().removeClass('active');
        $('.transparencia').removeClass('active');
        $('body, html').css('overflow', 'auto');
    });
    
    if($('.pop-up--solicitud').length) {
        document.addEventListener( 'wpcf7mailsent', function( event ) {
            $('.pop-up--solicitud .response-mail').css('display', 'block');
            $('.pop-up--solicitud .imagen').css('display', 'none');
            $('.pop-up--solicitud .title').css('display', 'none');
            $('.pop-up--solicitud .form').css('display', 'none');
        }, false );
    } else if($('.form--workshop').length) {
        document.addEventListener( 'wpcf7mailsent', function( event ) {
            $('.form--workshop .response-mail').css('display', 'block');
            $('.form--workshop .imagen').css('display', 'none');
            $('.form--workshop .title').css('display', 'none');
            $('.form--workshop .form').css('display', 'none');
            $('.form--workshop .info-workshop').css('display', 'none');
        }, false );
    }
 

    

    // Abrir Pop up

    $('.imagen-vertical-texto .info a').on('click', function(e) {
        e.preventDefault();
        // $('body, html').css('overflow', 'hidden');
        $('body, html').css('overflow', 'hidden');
        $('.transparencia').addClass('active');
        $('.pop-up--solicitud').addClass('active');
    });

    // Acceptance

    $('.pop-up--solicitud form .acceptance span.checkmark').on('click', function() {
        $(this).parent().toggleClass('active');
    });

    $('.form--workshop form .acceptance span.checkmark').on('click', function() {
        $(this).parent().toggleClass('active');
    });

    $('.formulario-contacto--content form .acceptance span.checkmark').on('click', function() {
        $(this).parent().toggleClass('active');
    });


    // ============================================================================== //
    // ======================        WORKSHOPS styles        ======================== //
    // ============================================================================== //

    $('body').on('click', '#show-more-workshops', function(e) {
        e.preventDefault();
        vermas();
    });

    $('body').on('click', '.form--workshop .cancell', function() {
        $('.form--workshop').removeClass('active');
        $('.form--workshop').find('.info-workshop ul.especificaciones').empty();
        $('body, html').css('overflow', 'auto');
        setTimeout(function() {
            $('.transparencia').removeClass('active');
        }, 700);
    });

    $('body').on('click', '.item-workshops .info a', function(e) {
        e.preventDefault();

        $('body, html').css('overflow', 'hidden');

        $('.transparencia').addClass('active');

        var titulo_ws = $(this).parent().find('h4').text();
        var espec_ws = $(this).parent().find('.especificaciones');
        var img_ws = $(this).parent().parent().find('.imagen').find('img').attr('src');

        $('.form--workshop').find('.imagen').css({'background-image': 'url(' + img_ws + ')'});
        $('.form--workshop').find('.title-workshop').find('h3').text(titulo_ws);
        $('.form--workshop .form-contact .text-1 input').val(titulo_ws);

        if(espec_ws != '') {
            $('.form--workshop').find('.info-workshop ul.especificaciones').css('display', 'flex');
            $('.form--workshop').find('.info-workshop ul.especificaciones').append(espec_ws.html());
        }

        setTimeout(function() {
            $('.form--workshop').addClass('active');
        }, 500);


    });


    // ============================================================================== //
    // =======================        HISTORIA styles        ======================== //
    // ============================================================================== //

    $('.carousel-history').owlCarousel({
        loop:false,
        nav:true,
        dots: false,
        smartSpeed: 600,
        navText: ["<img src='/wp-content/themes/peleman/img/angle-left.svg'>","<img src='/wp-content/themes/peleman/img/angle-right.svg'>"],
        responsive:{
            0:{
                center: true,
                items: 1,
                margin: 20,
            },
            768: {
                center: true,
                items: 1,
                margin: 40,
                stagePadding: 110,
                nav: false,
            },
            992: {
                center: true,
                items: 1,
                margin: 60,
                stagePadding: 160,
                nav: false,
            },
            1200: {
                center: true,
                items: 1,
                margin: 120,
                stagePadding: 220,
                nav: true,
            },
            1330: {
                center: true,
                items: 1,
                margin: 150,
                stagePadding: 250,
                nav: true,
            },
            1600: {
                center: true,
                items: 1,
                margin: 300,
                stagePadding: 380,
                nav: true,
            }
        }
    });

    $('.carousel-history').on('dragged.owl.carousel', function(e){
        e.preventDefault();
        
        // var customDots = $(this).parent().find('.carousel-dots');
        var realNumImages = $(this).find('.owl-item').length;
        var newImg = $(this).find('.active');
        var cantImgs = Math.ceil($(this).find('.owl-item').length);
        var excess = 0;
        
        if(realNumImages % 2 == 0 && realNumImages != 2) {
            var realIndex = (newImg.index() - excess < cantImgs) ? newImg.index() - excess : 0;
        } else if(realNumImages == 2) {
            // excess = 2;
            var realIndex = (newImg.index() - excess < cantImgs - 1) ? newImg.index() - excess : 0;
        } else {
            var realIndex = (newImg.index() - excess < cantImgs - 1) ? newImg.index() - excess : 0;
        }
        
        var items_carousel = $('.carousel-history .owl-item');
        var color = $(items_carousel[realIndex]).find('.item-history').attr('data-color');
        $('.srp-mainrow.history').css('background-color', color);

    });

    $('.carousel-history .owl-nav button.owl-next').on('click', function() {
        console.log('hola');
        $('.carousel-history').trigger('dragged.owl.carousel');
    });

    // ============================================================================== //
    // ====================        WOOCOMMERCE styles        ======================== //
    // ============================================================================== //


    // Separar páginas woocommerce distintas anchuras
    if($('.content--form-buy').length > 0 || $('.woocommerce-order-details').length > 0) {
        $('.woocommerce').addClass('main-wrapper');
    }

    if($('.icp_projects_list').length > 0) {
        if($('.icp_projects_list tbody tr').length < 2) {
            $('.icp_projects_list').css('display', 'none');
        }
    }

    if($('.woocommerce').length > 0) {
        $(".woocommerce form .form-row").each(function(i) {
            if($(this).find('label').length < 1) {
                $(this).addClass('without-label');
            } else if($(this).find('label').hasClass('screen-reader-text')) {
                $(this).addClass('without-label');
            }
        });
    }


    // Botones mas menos input number

    $('body').on('click', '.quantity .quantity-up', function() {
        var spinner = $(this).parent().parent();
        var input = spinner.find('input[type="number"]'),
            max = input.attr('max');
            var oldValue = parseFloat(input.val());
            if (max.length) {
                if (oldValue >= max) {
                var newVal = oldValue;
                } else {
                var newVal = oldValue + 1;
                }
            } else {
                var newVal = oldValue + 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
    });

    $('body').on('click', '.quantity .quantity-down', function() {
        var spinner = $(this).parent().parent();
        var input = spinner.find('input[type="number"]'),
            min = input.attr('min');
            var oldValue = parseFloat(input.val());
            if (oldValue <= min) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue - 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
    });

    // Fackear radiobuttons - ENVIO

    $('#shipping_method li').each(function(index, element) {
        if($(element).find('input').is(':checked')) {
            $(element).find('span').addClass('active');
        }
    });
    $('body').on('click', '#shipping_method li span', function(e) {
        e.preventDefault();
        $(this).parent().find('input[type="radio"]').trigger('click');
    });


    // Fackear - CUPON
    
    $('body').on('click', '.coupon-fake a.apply-coupon-fake', function(e) {
        e.preventDefault();
        var val_input = $(this).parent().find('input').val();
        $('.coupon input[type="text"]').val(val_input);
        $('.coupon button').trigger('click');
        $('html, body').animate({scrollTop: $(".woocommerce-cart-form").offset().top - 70 }, 1500);
        $(this).parent().find('input').val('');

    });

    // Fake radiobutton - ENVIAR A OTRA DIRECCIÓN

    $('body').on('change', '.woocommerce-form__label', function() {
        $(this).find('span.input').toggleClass('active');
    });


    // ============================================================================== //
    // ==========================        PRODUCT PAGE        ======================== //
    // ============================================================================== //

    $( window ).resize(function() {
        if($('.single-product--main').length) {
            if(window.innerWidth < 1200) {
                var altura_imagenes = $('#wooswipe').outerHeight();
                $('.fondo-prod').css('height', altura_imagenes + 160);
            }
        }
    });


    // ============================================================================== //
    // =========================        CATEGORY PAGE        ======================== //
    // ============================================================================== //

    // Sidebar Principal - Archive Page

    $('.sidebar-general .main-sidebar-general .arrow').on('click', function(e) {
        e.preventDefault();
        if($(this).hasClass('active')) {
            $('.sidebar-general .sidebar--nav-products').slideUp();
            $(this).removeClass('active');
        } else {
            $('.sidebar-general .sidebar--nav-products').slideDown();
            $(this).addClass('active');
        }
    });

    $('.sidebar-general .sidebar--nav-products .list-category .row-sidebar .arrow').on('click', function() {
        if($(this).hasClass('active')) {
            $(this).parent().parent().find('ul.list-category--second').slideUp();
            $(this).removeClass('active');
        } else {
            $(this).parent().parent().find('ul.list-category--second').slideDown();
            $(this).addClass('active');
        }
    });

    // Select2

    if($('select.orderby').length) {
        $('select.orderby').select2();
    }


    // ============================================================================== //
    // =========================        ANIMACIONES        ========================== //
    // ============================================================================== //

    if($('.carousel-home-products--content').length) {
        $('.carousel-home-products--content').each(function(index, element) {
            $(element).find('.owl-nav').find('button').on('mouseenter', function() {
                var class_btn = $(this).attr('class');
                $(this).parent().find(':not(.'+ class_btn +')').addClass('active');
            });
        });

        $('.carousel-home-products--content').each(function(index, element) {
            $(element).find('.owl-nav').find('button').on('mouseleave', function() {
                $(this).parent().find('button').removeClass('active');
            });
        });
    }


});


function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false;}
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
            if( accentFold(arr[i]['title'].toLowerCase()).includes(accentFold(val.toLowerCase())) ) {
                /*create a DIV element for each matching element:*/
                b = document.createElement("a");
                $(b).attr('href', arr[i]['link']);
                /*make the matching letters bold:*/

                var html = arr[i]['title'].substr(0, accentFold(arr[i]['title'].toLowerCase()).indexOf(accentFold(val.toLowerCase())));
                html += "<strong>" + arr[i]['title'].substr(accentFold(arr[i]['title'].toLowerCase()).indexOf(accentFold(val.toLowerCase())), val.length) + "</strong>";
                html += arr[i]['title'].substr(accentFold(arr[i]['title'].toLowerCase()).indexOf(accentFold(val.toLowerCase())) + val.length);

                // b.innerHTML = "<strong>" + arr[i]['title'].substr(indexOf(val), val.length) + "</strong>";
                // b.innerHTML += arr[i]['title'].substr(val.length);

                b.innerHTML = html;

                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i]['title'] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function(e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
          /*If the arrow DOWN key is pressed,
          increase the currentFocus variable:*/
          currentFocus++;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 38) { //up
          /*If the arrow UP key is pressed,
          decrease the currentFocus variable:*/
          currentFocus--;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 13) {
          /*If the ENTER key is pressed, prevent the form from being submitted,*/
          e.preventDefault();
          if (currentFocus > -1) {
            /*and simulate a click on the "active" item:*/
            if (x) x[currentFocus].click();
          }
        }
    });
    function addActive(x) {
      /*a function to classify an item as "active":*/
      if (!x) return false;
      /*start by removing the "active" class on all items:*/
      removeActive(x);
      if (currentFocus >= x.length) currentFocus = 0;
      if (currentFocus < 0) currentFocus = (x.length - 1);
      /*add class "autocomplete-active":*/
      x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
      /*a function to remove the "active" class from all autocomplete items:*/
      for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("autocomplete-active");
      }
    }
    function closeAllLists(elmnt) {
      /*close all autocomplete lists in the document,
      except the one passed as an argument:*/
      var x = document.getElementsByClassName("autocomplete-items");
      for (var i = 0; i < x.length; i++) {
        if (elmnt != x[i] && elmnt != inp) {
          x[i].parentNode.removeChild(x[i]);
        }
      }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}

function accentFold(inStr) {
    return inStr.replace(
      /([àáâãäå])|([ç])|([èéêë])|([ìíîï])|([ñ])|([òóôõöø])|([ß])|([ùúûü])|([ÿ])|([æ])/g, 
      function (str, a, c, e, i, n, o, s, u, y, ae) {
        if (a) return 'a';
        if (c) return 'c';
        if (e) return 'e';
        if (i) return 'i';
        if (n) return 'n';
        if (o) return 'o';
        if (s) return 's';
        if (u) return 'u';
        if (y) return 'y';
        if (ae) return 'ae';
      }
    );
}

function arrayProducts() {
    var array_productos = null;
    $.ajax({
        'async': false,
        'global': false,
        url: postlove.ajax_url,
        type: 'GET',
        data: {
            action: 'productos_array'
        },
        success: function(data) {
            array_productos = JSON.parse(data);
        }
    });
    return array_productos;
}


function vermas() {

    var n_elementos = $('.srp-mainrow.workshops .item-workshops').length;

    $.ajax({
        url: postlove.ajax_url,
        type: 'POST',
        data: {
            action: 'vermas',
            n_elementos : n_elementos,
        },
        success : function(response) {
            $( ".workshop--rest--content" ).append(response);
            if($('.cont-workshops').text() == $('.srp-mainrow.workshops .item-workshops').length) {
                $('#show-more-workshops').css('display', 'none');
            } else {
                console.log('mierda');
                console.log($('.cont-workshops').text());
                console.log($('.srp-mainrow.workshops .item-workshops').length);
            }
        }
    });
    return;
}