<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product; ?>


	<?php

	/**
	* Hook: woocommerce_before_single_product.
	*
	* @hooked wc_print_notices - 10
	*/
	do_action( 'woocommerce_before_single_product' );

	if ( post_password_required() ) {
		echo get_the_password_form(); // WPCS: XSS ok.
		return;
	}
	?>
	<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

		<div class="single-product--main">

			<div class="fondo-prod"></div>

			<div class="main-wrapper-product">

				<div class="single-product-main--content">

					<div class="box-dots"></div>

					<div class="texto-gigante"><h1>Create</h1></div>

					<?php
					/**
					* Hook: woocommerce_before_single_product_summary.
					*
					* @hooked woocommerce_show_product_sale_flash - 10
					* @hooked woocommerce_show_product_images - 20
					*/
					do_action( 'woocommerce_before_single_product_summary' );
					?>

					<div class="summary entry-summary">
						<?php
						/**
						* Hook: woocommerce_single_product_summary.
						*
						* @hooked woocommerce_template_single_title - 5
						* @hooked woocommerce_template_single_rating - 10
						* @hooked woocommerce_template_single_price - 10
						* @hooked woocommerce_template_single_excerpt - 20
						* @hooked woocommerce_template_single_add_to_cart - 30
						* @hooked woocommerce_template_single_meta - 40
						* @hooked woocommerce_template_single_sharing - 50
						* @hooked WC_Structured_Data::generate_product_data() - 60
						*/
						do_action( 'woocommerce_single_product_summary' );
						?>
					</div>

				</div>

			</div>

		</div>

		<?php
		/**
		* Hook: woocommerce_after_single_product_summary.
		*
		* @hooked woocommerce_output_product_data_tabs - 10
		* @hooked woocommerce_upsell_display - 15
		* @hooked woocommerce_output_related_products - 20
		*/
		do_action( 'woocommerce_after_single_product_summary' );
		?>

		
	</div>

	<?php do_action( 'woocommerce_after_single_product' ); ?>


<div class="single-product--block">

	<?php if(get_field('caracteristicas_producto')) { ?>
		<div class="block-caracteristicas">
			<h5><?php echo pll__('Características', 'Peleman'); ?></h5>
			<div class="block-caracteristicas--content">
				<?php foreach(get_field('caracteristicas_producto') as $character) { ?>
					<div class="caracter"><p><?php echo $character['caracteristica']; ?></p></div>
				<?php } ?>
			</div>
		</div>
	<?php } ?>

	<?php if(get_field('video_producto')) { ?>
		<div class="block-video">
			<div class="main-wrapper">
				<div class="block-video--content">
					<div class="video">
						<img src="<?php echo get_template_directory_uri(); ?>/img/woo-icons/play.svg" alt="">
						<iframe width="100%" class="iframe-video" playsinline height="315" src="<?php echo get_field('video_producto'); ?>"></iframe>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>

	<?php if(get_field('banner_producto')) { ?>
		<?php foreach(get_field('banner_producto') as $banner) { ?>
			<div class="block-banner">
				<div class="main-wrapper-1340">
					<div class="imagen-texto-color-fondo">
						<div class="imagen" style="background-image:url(<?php echo $banner['imagen_banner']; ?>)"></div>
						<div class="info" style="background-color: <?php echo $bloque['color_fondo_itcf']; ?>" >
							<div class="box-text">
								<h1><?php echo $banner['titulo_banner']; ?></h1>
							</div>
							<div class="text-general-bloques">
								<?php echo $banner['texto_banner']; ?>                                
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	<?php } ?>

	<div class="block-related">

		<div class="main-wrapper-right">
			<div class="fondo"></div>
			<div class="section-home-products--content">
				<?php  
					$cat = get_the_terms( $product->get_id(), 'product_cat' );

					foreach ($cat as $categoria) {
						if($categoria->parent == 0){
							$parent_category = $categoria;
						}
					}
				?>
				<div class="box-text">
					<h1><?php echo pll__('Más en', 'Peleman'); ?> <?php echo $parent_category->name; ?></h1>
				</div>
				<div class="text-secondary">
					<h3><?php echo pll__('Escoge entre nuestros productos y empieza a personalizar.', 'Peleman'); ?></h3>
				</div>
				<div class="carousel-home-products--content">
					<div class="box-dots"></div>
					<div class="carousel-single-products owl-carousel owl-theme">
						<?php
						$args = array (
							'post_type' => 'product',
							'posts_per_page' => -1,
						);
			  
						$query = new WP_Query($args);
			  
						if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>

							<a href="<?php echo get_the_permalink(); ?>" style="background-image:url(<?php echo get_the_post_thumbnail_url(); ?>)" class="item-product">
								<div class="opacity">
									<h3><?php echo get_the_title(); ?></h3>
								</div>
							</a>

						<?php endwhile; endif; wp_reset_postdata(); ?>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>


