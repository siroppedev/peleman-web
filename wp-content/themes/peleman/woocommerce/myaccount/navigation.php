<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_account_navigation' );
?>

<nav class="woocommerce-MyAccount-navigation">
	<ul>
		<?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : ?>
			<li class="<?php echo wc_get_account_menu_item_classes( $endpoint ); ?>">
				<?php if($endpoint == 'icp_projects'){ ?>
					<a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint )); ?>">
					<img src="<?php echo get_template_directory_uri() ?>/img/menu-usuario/001-files-and-folders.svg" alt="">
					<p><?php echo pll__('Proyectos', 'Peleman'); ?></p>
					</a>
				<!-- empty -->
				<?php } else if($endpoint == 'orders') { ?>
					<a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint )); ?>">
					<img src="<?php echo get_template_directory_uri() ?>/img/menu-usuario/002-shopping-cart-black-shape.svg" alt="">
					<p><?php echo pll__('Pedidos', 'Peleman'); ?></p>
					</a>
				<?php } else if($endpoint == 'edit-address') { ?>
					<a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint )); ?>">
					<img src="<?php echo get_template_directory_uri() ?>/img/menu-usuario/003-map.svg" alt="">
					<p><?php echo pll__('Direcciones', 'Peleman'); ?></p>
					</a>
				<?php } else if($endpoint == 'edit-account') { ?>
					<a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint )); ?>">
					<img src="<?php echo get_template_directory_uri() ?>/img/menu-usuario/004-user.svg" alt="">
					<p><?php echo pll__('Cuenta', 'Peleman'); ?></p>
					</a>
				<?php } else if($endpoint == 'customer-logout') { ?>
					<a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint )); ?>">
					<img src="<?php echo get_template_directory_uri() ?>/img/menu-usuario/005-shut-down-icon.svg" alt="">
					<p><?php echo pll__('Salir', 'Peleman'); ?></p>
					</a>
				<?php } ?>
			</li> 
		<?php endforeach; ?>
	</ul>
</nav>

<?php do_action( 'woocommerce_after_account_navigation' ); ?>
