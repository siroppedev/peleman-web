<?php get_header(); ?>

<div id="main-content" class="main-content">
	<div id="primary" class="content-area srp-mainrow">
		<div id="content" class="site-content" role="main">

			<?php
				// Start the Loop.
			while ( have_posts() ) :
				the_post();
				the_content();
			endwhile;
			?>

		</div>
	</div>
</div>

<?php get_footer(); ?>