<?php 

/**
 * Template Name: Page Home
 *
 */

get_header(); ?>

    <div class="srp-mainrow home">

        <div class="srp-section section-home-top">
            <div class="main-wrapper-left">
                <div class="fondo"></div>
                <div class="section-home-top--content">
                    <div class="box-text">
                        <h1><?php echo get_field('texto_producto_destacado'); ?></h1>
                    </div>
                    <div class="content--product">
                        <div class="box-dots"></div>
                        <div class="box-dots"></div>
                        <div class="content--product--img" style="background-image:url(<?php echo get_field('imagen_producto_destacado'); ?>)">
                            <div class="box-info">
                                <?php echo get_field('texto_box_producto_destacado'); ?>
                                <a class="btn-primary" href="<?php echo get_field('link_producto_destacado'); ?>"><p><?php echo pll__('Empieza a crear', 'Peleman'); ?></p></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="srp-section section-home-valors">
            <div class="main-wrapper">
                <div class="section-home-valors--content">
                    <div class="carousel-valors-home carousel-valors-home-desktop">
                        <?php foreach(get_field('valores') as $valores) { ?>
                            <div class="item-valors">
                                <div class="circle-img">
                                    <div class="imagen">
                                        <img src="<?php echo $valores['imagen']; ?>" alt="">
                                    </div>
                                </div>
                                <?php echo $valores['texto']; ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="carousel-valors-home owl-carousel owl-theme carousel-valors-home-mobile">
                        <?php foreach(get_field('valores') as $valores) { ?>
                            <div class="item-valors">
                                <div class="circle-img">
                                    <div class="imagen">
                                        <img src="<?php echo $valores['imagen']; ?>" alt="">
                                    </div>
                                </div>
                                <?php echo $valores['texto']; ?>
                            </div>
                        <?php } ?>
                    </div>
                    <?php  ?>
                    <a href="/nuestro-valor" class="btn-primary">
                        <p><?php echo pll__('Nuestros valores', 'Peleman'); ?><span></span></p>
                    </a>
                </div>
            </div>
        </div>

        <?php get_template_part( 'template-parts/template', 'destacados' ); ?>

    </div>


<?php get_footer();?>