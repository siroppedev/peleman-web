<?php
/**
 * peleman functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package peleman
 */

//   require get_template_directory() . '/inc/init.php';

  if ( ! function_exists( 'peleman_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	 function peleman_widgets_init() {
		register_sidebar( array(
			'name'          => esc_html__( 'Sidebar_newsletter', 'peleman' ),
			'id'            => 'sidebar-news',
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		) );
	}
	add_action( 'widgets_init', 'peleman_widgets_init' );
	function peleman_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on peleman, use a find and replace
		 * to change 'peleman' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'peleman', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu_principal' => esc_html__( 'Main menu', 'peleman' ),
			'menu_icons' => esc_html__( 'Menu Iconos', 'peleman' ),
			'idiomas' => esc_html__( 'Language', 'peleman' ),
			'menu_secundario' => esc_html__( 'Second menu', 'peleman' ),
			'menu_footer_empresa' => esc_html__( 'Footer Empresa menu', 'peleman' ),
			'menu_footer_servicios' => esc_html__( 'Footer servicios menu', 'peleman' ),
			'menu_footer_ayuda' => esc_html__( 'Footer ayuda menu', 'peleman' ),
			'menu_footer_legal' => esc_html__( 'Footer legal menu', 'peleman' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'peleman_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'peleman_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function peleman_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'peleman_content_width', 640 );
}
add_action( 'after_setup_theme', 'peleman_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */

/**
 * Enqueue scripts and styles.
 */
function peleman_scripts() {
	wp_enqueue_style( 'peleman-style', get_stylesheet_uri() );
    wp_enqueue_style( 'carousel-default-css', get_template_directory_uri() . '/css/owl.theme.default.min.css' );
    wp_enqueue_style( 'carousel-css', get_template_directory_uri() . '/css/owl.carousel.min.css' );
    wp_enqueue_style( 'select-css', get_template_directory_uri() . '/css/select2.min.css' );
    wp_enqueue_style( 'peleman-css', get_template_directory_uri() . '/css/siroppe.css' );
    
    wp_enqueue_script( 'carousel-js-main', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '1', true );
    wp_enqueue_script( 'scrollmagic-js', get_template_directory_uri() . '/js/ScrollMagic.min.js', array('jquery'), '1', true );
    wp_enqueue_script( 'select-js', get_template_directory_uri() . '/js/select2.full.min.js', array('jquery'), '1', true );
	// wp_enqueue_script( 'scrollbar-js', get_template_directory_uri() . '/js/jquery.scrollbar.js', array('jquery'), '1', true );
	wp_enqueue_script( 'scrollmagic-indicators-js', get_template_directory_uri() . '/js/debug.addIndicators.js', array('jquery'), '1', true );

    wp_enqueue_script( 'peleman-main', get_template_directory_uri() . '/js/main.js', array('jquery'), '1', true );
    
	wp_localize_script( 'peleman-main', 'postlove', array(
		'ajax_url' => admin_url( 'admin-ajax.php' )
	));

}

add_action( 'wp_enqueue_scripts', 'peleman_scripts' );




// Añadir Páginas del tema de woocommerce

function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );



/**
 * Change number of products that are displayed per page (shop page)
 */
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = 9;
  return $cols;
}


// Redirect to Logout

add_action('wp_logout','auto_redirect_after_logout');

function auto_redirect_after_logout(){

	wp_redirect( home_url() );
	exit();

}

// Array productos buscador 

function productos_array(){
	
	$array_products = array();
	
    $args = array (
		'post_type' => 'product',
        'posts_per_page' => -1,
    );
	$query = new WP_Query($args);
	
    if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); 
	
	$array_products[] = array(
		'link' => get_the_permalink(),
		'title' => get_the_title()
	);
	
	endwhile; endif; wp_reset_postdata();

	// var_dump($array_products);
	$array_products = json_encode($array_products);
	echo ($array_products);
	die();

}


add_action( 'wp_ajax_nopriv_productos_array', 'productos_array' );
add_action( 'wp_ajax_productos_array', 'productos_array' );



// Funcion para ver más workshops

function vermas(){

	$todays_date = date("d-m-Y");
	$today = strtotime($todays_date);
	
	$args = array (
		'post_type'         => 'workshops',
		'posts_per_page'    => 2,
		'offset'           => $_POST['n_elementos'],
	);

	$query = new WP_Query($args);
	if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); 
	
	$exp_date = get_field('fecha_de_evento');
	$exp_date = str_replace('/', '-', $exp_date); 
	$expiration_date = strtotime($exp_date); ?>

		<div class="item-workshops <?php echo ($expiration_date < $today)?'desactive':'';?>">
			<div class="imagen" style="background-image:url(<?php echo get_the_post_thumbnail_url(); ?>)">
				<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
			</div>
			<div class="info">
				<h4><?php echo get_the_title(); ?></h4>
				<ul class="especificaciones">
					<?php foreach(get_field('especificaciones') as $esp) { ?>
						<li>
							<img src="<?php echo $esp['logo']; ?>" alt="">
							<p><?php echo $esp['texto']; ?></p>
						</li>
					<?php } ?>
				</ul>
				<div class="texto">
					<?php $text = apply_filters('the_content', get_post(get_the_ID())->post_content);
					echo $text; ?>
				</div>
				<a href="" class="btn-secondary"><p><?php echo pll__('Reservar', 'Peleman'); ?></p></a>
			</div>
		</div>

	<?php endwhile; endif; wp_reset_postdata(); die();
}


add_action( 'wp_ajax_nopriv_vermas', 'vermas' );
add_action( 'wp_ajax_vermas', 'vermas' );


// function isa_woo_cart_attributes($cart_item, $cart_item_key) {
//     global $product; 
//     if (is_cart()){ 
//         echo "<style>#checkout_thumbnail{display:none;}</style>"; 
//     } 
//     $item_data = $cart_item_key['data']; 
//     $post = get_post($item_data->id); 
//     $thumb = get_the_post_thumbnail($item_data->id, array( 32, 50)); 
//     echo '<div id="checkout_thumbnail" style="float: left; padding-right: 8px">' . $thumb . '</div> '; 
// } 
// add_filter('woocommerce_cart_item_name', isa_woo_cart_attributes, 10, 2);