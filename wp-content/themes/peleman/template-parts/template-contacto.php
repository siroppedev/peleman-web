<?php $id_contacto = 10179; ?>


<div class="main-wrapper">
    <div class="cuatro-bloques--content">
        <?php foreach(get_field('cuatro_bloques', $id_contacto) as $item_bloque) { ?>
            <?php if($item_bloque['tipo_contacto'] == '') { ?>
                <div class="item-bloque-contacto">
            <?php } else if($item_bloque['tipo_contacto'] == 'telefono') { ?>
                <a href="tel:<?php echo $item_bloque['subtitulo_link_contacto'] ?>" class="item-bloque-contacto">
            <?php } else if($item_bloque['tipo_contacto'] == 'correo') { ?>
                <a href="mailto:<?php echo $item_bloque['subtitulo_link_contacto'] ?>" class="item-bloque-contacto">
            <?php } else if($item_bloque['tipo_contacto'] == 'direccion') { ?>
                <a target="_blank" href="<?php echo $item_bloque['subtitulo_link_contacto'] ?>" class="item-bloque-contacto">
            <?php } else { ?>
                <div class="item-bloque-contacto">                                    
            <?php } ?>
                <img src="<?php echo $item_bloque['icono_contacto'] ?>" alt="">
                <h3><?php echo $item_bloque['titulo_contacto'] ?></h3>
                <p><?php echo $item_bloque['subtitulo_contacto'] ?></p>
            <?php if($item_bloque['tipo_contacto'] != '') { ?>
                </a>
            <?php } else { ?>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</div>