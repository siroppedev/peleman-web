<?php $id_home = 6310; ?>

<div class="srp-section section-home-products">
    <div class="main-wrapper-right">
        <div class="fondo"></div>
        <div class="section-home-products--content">
            <div class="box-text">
                <h1><?php echo get_field('titulo_productos_destacados', $id_home); ?></h1>
            </div>
            <div class="text-secondary">
                <h3><?php echo get_field('subtitulo_productos_destacados', $id_home); ?></h3>
            </div>
            <div class="carousel-home-products--content">
                <div class="box-dots"></div>
                <div class="carousel-home-products owl-carousel owl-theme">
                    <?php foreach(get_field('productos_destacados', $id_home) as $product) { ?>
                        <a href="<?php echo get_the_permalink($product); ?>" style="background-image:url(<?php echo get_the_post_thumbnail_url($product); ?>)" class="item-product">
                            <div class="opacity">
                                <h3><?php echo get_the_title($product); ?></h3>
                            </div>
                        </a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>