<div class="testimonios--template">
    <div class="main-wrapper">
        <div class="testimonios--content">
            <h1><?php echo $title; ?></h1>
            <div class="testimonios-carousel owl-carousel owl-theme">
                <?php $args = array (
                    'post_type' => 'testimonios',
                    'posts_per_page' => -1,
                );
                $query = new WP_Query($args);
                if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
                    <div class="item-testimonio">
                        <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                        <h4><?php echo get_the_title(); ?></h4>
                        <?php $text = apply_filters('the_content', get_post(get_the_ID())->post_content);
                        echo $text; ?>
                    </div>
                <?php endwhile; endif; wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</div>