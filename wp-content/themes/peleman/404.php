<?php 

get_header(); ?>

    <div class="srp-mainrow general-bloques error">

        <div class="srp-section general-bloque error-page">
            <div class="main-wrapper-1000">
                <div class="texto-gigante-con-imagen">
                    <div class="texto-gigante"><h1>404</h1></div>
                    <div class="imagen" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/error-page.jpg)"></div>
                    <div class="info">
                        <div class="text-general-bloques">
                            <h3>¡Ups! Esta página no existe o se ha modificado.</h3>
                            <p>Vuelve a la home y sigue descubriendo todos nuestros productos y servicios.</p>
                        </div>
                        <a class="btn-primary" href="/">
                            <p>Volver a la home</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>


<?php get_footer();?>