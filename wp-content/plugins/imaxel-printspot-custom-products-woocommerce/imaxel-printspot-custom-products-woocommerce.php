<?php
/**
* Plugin Name: Imaxel Printspot Custom Products Woocommerce
* Plugin URI: http://www.imaxel.com
* Description: A wordpress plugin to integrate Printspot Custom Products with the woocommerce catalogue.
* Version: 1.0.0
* WC requires at least: 3.0.0
* WC tested up to: 3.3.4
* Text Domain: printspot-plugins
* Domain Path: /language/
* Author: Imaxel
* Author URI: http://www.imaxel.com
* License: All right reserved 2018
*/

if ( ! class_exists( 'Printspot_Custom_Products_Woocommerce' ) ) {

    class Printspot_Custom_Products_Woocommerce {

        public function __construct() {

            //region funciones de configuracion generica del plugin
            register_activation_hook(__FILE__, array($this, 'imaxel_icp_woocommerce_install'));

            //region funciones de configuracion generica de woocommerce
            add_filter('woocommerce_settings_tabs_array', __CLASS__ . '::add_settings_tab', 50);
            add_action('woocommerce_settings_tabs_settings_tab_imaxel_icp', __CLASS__ . '::settings_tab');
            add_action('woocommerce_update_options_settings_tab_imaxel_icp', __CLASS__ . '::update_settings');

            //region funciones de configuracion del producto de woocommerce
            add_filter('woocommerce_product_data_tabs', array($this, 'imaxel_icp_product_data_tab'));
            add_action('woocommerce_product_data_panels', array($this, 'imaxel_icp_product_data_fields'));
            add_action('woocommerce_process_product_meta', array($this, 'imaxel_icp_product_data_fields_save'));

            //region funciones de configuracion de la vista del producto de woocommerce
            add_action('woocommerce_single_product_summary', array($this, 'imaxel_icp_start_customizing'));
            add_action('save_post', array($this, 'imaxel_icp_product_save'));

            //add icp-woocommerce-operations
            include_once('includes/icp-woocommerce-operations.php');
        }

        //==============================region funciones de configuracion generica del plugin=================================//
        //====================================================================================================================//

        /**
         * generate icp_products_woocommerce
         */
        function imaxel_icp_woocommerce_install() {
            global $wpdb;

            $table_name = $wpdb->prefix . 'icp_products_woocommerce';
            $charset_collate = $wpdb->get_charset_collate();

            $sql = "CREATE TABLE $table_name (
                `id` int(10) NOT NULL AUTO_INCREMENT,
                `code` varchar(255) CHARACTER SET utf8 NOT NULL,
                `name` varchar(255) CHARACTER SET utf8 NOT NULL,
                `site` varchar(255) CHARACTER SET utf8 NOT NULL,
                PRIMARY KEY (`id`)

            ) $charset_collate;";

            //create table
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);
        }
        
        //============================region funciones de configuracion generica de woocommerce===============================//
        //====================================================================================================================//

        /**
         * add woocommerce settings tab
         */
        public static function add_settings_tab($settings_tabs) {
            $settings_tabs['settings_tab_imaxel_icp'] = __('Imaxel ICP', 'printspot-plugins');
            return $settings_tabs;
        }

        /**
         * add woocommerce settings to the new tab
         */
        public static function settings_tab() {
            woocommerce_admin_fields(self::get_icp_settings());
        }

        /**
         * define new settings
         */
        public static function get_icp_settings() {
            $settings = array(
                    'section_title' => array(
                        'name' => __('ICP API Conection', 'printspot-plugins'),
                        'type' => 'title',
                        'desc' => __('Enter the endpoint, public key and private key and save changes to sincronize your woocommerce with your ICP products.', 'printspot-plugins'),
                        'id' => 'wc_settings_tab_imaxel_icp_section_title'
                    ),
                    'endpoint' => array(
                        'name' => __('Endpoint', 'printspot-plugins'),
                        'type' => 'text',
                        'css' => 'min-width:300px;',
                        'desc' => __('Introduce the name of your printspot account', 'printspot-plugins'),
                        'id' => 'wc_settings_tab_imaxel_icp_endpoint'
                ),
                    'publickey' => array(
                        'name' => __('Public key', 'printspot-plugins'),
                        'type' => 'text',
                        'css' => 'min-width:300px;',
                        'desc' => __('Introduce Public key supplied by Imaxel', 'printspot-plugins'),
                        'id' => 'wc_settings_tab_imaxel_icp_publickey'
                    ),
                    'privatekey' => array(
                        'name' => __('Private key', 'printspot-plugins'),
                        'type' => 'text',
                        'css' => 'min-width:300px;',
                        'desc' => __('Introduce Private key supplied by Imaxel', 'printspot-plugins'),
                        'id' => 'wc_settings_tab_imaxel_icp_privatekey'
                    ),
                    'interface_color' => array(
                        'name' => __('Theme Color', 'printspot-plugins'),
                        'type' => 'color',
                        'css' => 'min-width:300px;',
                        'desc' => __('Introduce the hexadecimal color code to user on your plugin interface (ex: #010101)', 'printspot-plugins'),
                        'id' => 'wc_settings_tab_imaxel_icp_color_theme'
                    ),
                    'section_end' => array(
                        'type' => 'sectionend',
                        'id' => 'wc_settings_tab_imaxel_icp_section_end'
                    ),

            );
            return apply_filters('wc_settings_tab_imaxel_icp_settings', $settings);
        }

        /**
         * set update settings function
         */
        public static function update_settings() {
            woocommerce_update_options(self::get_icp_settings());
            Printspot_Custom_Products_Woocommerce::update_icp_products();
        }

        /**
         * define update settings function
         */
        private static function update_icp_products() {
            
            //get products list from ICP API
            $APIendpoint = get_option('wc_settings_tab_imaxel_icp_endpoint'); 
            if(!empty($APIendpoint)) {
                //$endpoint = 'http://test.printspot.io/'.$APIendpoint.'/';
                $endpoint = 'http://localhost/printspot/'.$APIendpoint.'/';
                $ICPWimaxelOperations = new ICPW_operations();
                $icpProductsJSON = $ICPWimaxelOperations->getIcpProducts($endpoint);
                $icpProducts = json_decode($icpProductsJSON);

                //save products list on local database
                global $wpdb;
                $siteID = $icpProducts->site_origin;
                $icpWoocommerceProductsTable = $wpdb->prefix.'icp_products_woocommerce';
                foreach($icpProducts->products as $product) {
                    $sql = "
                        INSERT INTO ".$icpWoocommerceProductsTable."
                            (code, name, site)
                        VALUES 
                            ($product->id, '$product->name', $siteID)
                    ";
                    $saveProducts = $wpdb->query($sql);
                }
            }
        }

        //===========================region funciones de configuracion de producto de woocommerce=============================//
        //====================================================================================================================//

        /**
         * add woocommerce product config tab
         */
        function imaxel_icp_product_data_tab($product_data_tabs) {
            $product_data_tabs['imaxel-icp-product-tab'] = array(
                'label' => __('ICP Imaxel', 'printspot-plugins'),
                'target' => 'imaxel_icp_product_data'

            );
            return $product_data_tabs;
        }

        /**
         * add woocommerce product config tab content
         */
        public function imaxel_icp_product_data_fields() {
            
            //get icp saved products
            global $wpdb;
            global $post;
            $icpWoocommerceProductsTable = $wpdb->prefix.'icp_products_woocommerce';
            $icpProducts = $wpdb->get_results("SELECT * FROM ".$icpWoocommerceProductsTable);
            foreach($icpProducts as $product) {
                $icpProductsArray[0] = '';
                $icpProductsArray[$product->code] = $product->name; 
            }
            
            //selected product
            $selectedProduct = get_post_meta($post->ID, "_imaxel_icp_products", true);

            //show products list
            ?><div id="imaxel_icp_product_data" class="panel woocommerce_options_panel" style="padding-left:15px;padding-top:15px"><?php
            ?>ICP Products<?php
            woocommerce_wp_select(array(
                    'id' => '_imaxel_icp_products',
                    'class' => '_imaxel_icp_products',
                    'label' => __('Select a product', 'printspot-plugins'),
                    'options' => $icpProductsArray,
                    'value' => $selectedProduct
                )
            );
            ?></div><?php
        }

        /**
         * save product info as meta
         */
        function imaxel_icp_product_data_fields_save($post_id) {
            update_post_meta($post_id, '_imaxel_icp_products', $_POST['_imaxel_icp_products']);
        }

        //=====================region funciones de configuracion de vista del producto de woocommerce=========================//
        //====================================================================================================================//
        
        /**
         * add customize button to product catalogue
         */
        function imaxel_icp_start_customizing() {
            
            global $product;
            global $wpdb;

            $icpProduct = get_post_meta($product->get_id(), "_imaxel_icp_products", true);

            if(!empty($icpProduct)) {
                $wproduct = $product->get_id();
            }

            if(!empty($icpProduct)) {

                //remove woocommerce add to cart button
                remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
                ?>
                    <script>
                        jQuery('form.cart').hide();
                    </script>
                <?php

                //get ICP url page
                $icpURL = get_option('icp_url');

                //get product data;
                $icpWoocommerceProducstTable = $wpdb->prefix.'icp_products_woocommerce'; 
                $icpProductData = $wpdb->get_row("SELECT site FROM ".$icpWoocommerceProducstTable." WHERE code=$icpProduct");
                $icpFinalURL = $icpURL.'?id='.$icpProduct.'&site='.$icpProductData->site.'&block=1';
                if(isset($wproduct)) {$icpFinalURL .= '&wproduct='.$wproduct;} 
                echo '<a href="'.$icpFinalURL.'"><button style="margin-bottom: 15px;" class="single_add_to_cart_button button alt">'.__('Customize','printspot-plugins').'</button></a>';
            }
        }

        /**
         * save product as simple
         */
        function imaxel_icp_product_save($post_id) {

            $product = wc_get_product($post_id);
            $selectedProduct = get_post_meta($post_id, "_imaxel_icp_products", true);

            if ($product && $selectedProduct > 0) {
                wp_set_object_terms($post_id, 'simple', 'product_type');
            }

        }
    }
}

// finally instantiate our plugin class and add it to the set of globals
$GLOBALS['Printspot_Custom_Products_Woocommerce'] = new Printspot_Custom_Products_Woocommerce();