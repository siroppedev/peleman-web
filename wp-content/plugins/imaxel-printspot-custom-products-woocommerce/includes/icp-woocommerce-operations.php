<?php

class ICPW_operations {

    public function getIcpProducts($endpoint) {
        $products = $this->doRequest($endpoint);
        return $products;
    }

    private function doRequest($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (FALSE === $content)
            throw new Exception(curl_error($ch), curl_errno($ch));
        curl_close($ch);
        return $content;
    }

}

?>