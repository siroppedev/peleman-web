<?php
/**
* Plugin Name: Imaxel Printspot Custom Products
* Plugin URI: http://www.imaxel.com
* Description: A wordpress plugin to integrate Printspot Custom Products.
* Version: 1.0.0
* WC requires at least: 3.0.0
* WC tested up to: 3.3.4
* Text Domain: printspot-plugins
* Domain Path: /language/
* Author: Imaxel
* Author URI: http://www.imaxel.com
* License: All right reserved 2018
*/

if ( ! class_exists( 'Printspot_Custom_Products' ) ) {

    class Printspot_Custom_Products {

        public function __construct() {

            //start php session
            add_action('init', array($this, 'start_icp_session'));

            //generate pluguin tables
            register_activation_hook(__FILE__, array($this, 'icp_install'));
            add_action('init', array($this, 'icp_install'));

            //load text domain
            add_action('plugins_loaded',array($this, 'load_textdomain'));

            //load vue.js
            add_action( 'wp_enqueue_scripts', array($this, 'icpLoadScripts' ));

            //add shortcode with icp_views
            add_shortcode( 'icp_view' , array($this, 'load_icp_view'));

            //generate rewrite rules and query vars
            add_filter('query_vars', array($this, 'icpQueryVars'));

            //add icpOperations
            include_once('includes/imaxel-icp-operations.php');

            //cart
            add_action('woocommerce_before_calculate_totals', array($this, 'icp_custom_cart_data'),10);

            //=============WOOCOMMERCE FUNCTIONS=============//
            //register new endpoint
            add_action( 'init', array($this, 'my_account_new_endpoints' ));
            //get new endpoint content
            add_action( 'woocommerce_account_icp_projects_endpoint', array($this, 'icp_projects_endpoint_content' ));
            //add endpoint to my account menu
            add_filter ( 'woocommerce_account_menu_items', array($this, 'icp_my_account_menu_order' ), 15);
            //generate new icorder on new woocommerce order processing
            add_action('woocommerce_order_status_processing', array($this, 'newICOorder' ));
            //add_action('woocommerce_before_checkout_form', array($this, 'newICOorder' ));

            //==============REGISTER AJAX CALLS==============//
            //create project
            add_action( 'wp_ajax_nopriv_createNewIcpProject', array($this, 'createNewIcpProject' ));
            add_action( 'wp_ajax_createNewIcpProject', array($this, 'createNewIcpProject' ));

            //validate pdf
            add_action( 'wp_ajax_nopriv_validatePdfUploader', array($this, 'validatePdfUploader' ));
            add_action( 'wp_ajax_validatePdfUploader', array($this, 'validatePdfUploader' ));
            
            //save pdf url
            add_action( 'wp_ajax_nopriv_savePdfUploader', array($this, 'savePdfUploader' ));
            add_action( 'wp_ajax_savePdfUploader', array($this, 'savePdfUploader' ));

            //open the editor
            add_action( 'wp_ajax_nopriv_openEditor', array($this, 'openEditor' ));
            add_action( 'wp_ajax_openEditor', array($this, 'openEditor' ));

            //confirm project design
            add_action( 'wp_ajax_nopriv_confirmProjectDesign', array($this, 'confirmProjectDesign' ));
            add_action( 'wp_ajax_confirmProjectDesign', array($this, 'confirmProjectDesign' ));

            //add item to cart
            add_action( 'wp_ajax_nopriv_icpAddItemToCart', array($this, 'icpAddItemToCart' ));
            add_action( 'wp_ajax_icpAddItemToCart', array($this, 'icpAddItemToCart' ));

            //return to block
            add_action( 'wp_ajax_nopriv_returnToBlock', array($this, 'returnToBlock' ));
            add_action( 'wp_ajax_returnToBlock', array($this, 'returnToBlock' ));

            //save project
            add_action( 'wp_ajax_nopriv_saveICPproject', array($this, 'saveICPproject' ));
            add_action( 'wp_ajax_saveICPproject', array($this, 'saveICPproject' ));
        }

        //==========PLUGIN GENERIC FUNCTIONS==========//
        //============================================//

        /**
         * start icp session
         */
        function start_icp_session() {
            if( !session_id() ) {
                session_start();
            }
        }

        /**
         * load printspot-plugins text domain
         */
        function load_textdomain() {            
            $locale = apply_filters( 'plugin_locale', get_locale(), 'printspot-plugins' );
            $loadTextDomain = load_textdomain( 'printspot-plugins', WP_LANG_DIR . '/printspot-plugins-' . $locale . '.mo' );
        }

        /**
         * load vue .js
         */
        function icpLoadScripts()  {
            wp_enqueue_script('vue','https://cdn.jsdelivr.net/npm/vue/dist/vue.js', TRUE);
            wp_enqueue_script('font-awesome', 'https://kit.fontawesome.com/86d5966c90.js', TRUE);
            wp_enqueue_style('icp-product', plugins_url('/assets/styles.css', __FILE__));
            wp_enqueue_script('icp_script', plugins_url('/assets/main.js', __FILE__), array('jquery'), TRUE);
            wp_localize_script('icp_script', 'ajax_object', array('url' => admin_url('admin-ajax.php')));
        }

        /**
         * generate plugin tables
         */
        function icp_install() {
            global $wpdb;
            $charset_collate = $wpdb->get_charset_collate();
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

            //create wp_imaxel_printspot_custom_products_products
            $table_name = $wpdb->prefix.'icp_products_projects';
            $sql = "CREATE TABLE $table_name (
                `id` int(10) NOT NULL AUTO_INCREMENT,
                `product` varchar(255) CHARACTER SET utf8,
                `product_name` varchar(255) CHARACTER SET utf8,
                `woo_product` int(10),
                `variation` int(10),
                `variation_price` int(10),
                `first_block` int(10),
                `site` int(10),
                `user` int(10),
                `date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
                PRIMARY KEY (`id`)
            ) $charset_collate;";

            //create table
            dbDelta($sql);

            //create imaxel_printspot_custom_products_projects_components
            $table_name = $wpdb->prefix.'icp_products_projects_components';
            $sql = "CREATE TABLE $table_name (
                `id` int(10) NOT NULL AUTO_INCREMENT,
                `project` int(10),
                `block` int(10),
                `value` longtext CHARACTER SET utf8,
                PRIMARY KEY (`id`),
                CONSTRAINT ".$wpdb->prefix."icp_project_comp_proj FOREIGN KEY (project) REFERENCES ".$wpdb->prefix."icp_products_projects(id) ON DELETE CASCADE ON UPDATE RESTRICT
            ) $charset_collate;";

            //create table
            dbDelta($sql);
        }

        /**
         * add query vars
         */
        function icpQueryVars($query_vars) {
            $query_vars[] = 'id';
            $query_vars[] = 'site';
            $query_vars[] = 'block';
            $query_vars[] = 'wproduct';
            return $query_vars;
        }

        /**
         * get current block type
         */
        function getCurrentBlogType($productData) {
            global $wpdb;
            $blockID = intval($_GET['block']);
            $siteID = $_GET['site'];
            foreach($productData->blocks as $block) {
                $productBlocks[$block->definition->block_id] = $block->definition;
            }
            $blockType = $productBlocks[$blockID]->block_type;
            return $blockType;
        }

        /**
         * generate shortcode to add in a page
         */
        function load_icp_view() {
            
            //get permalink where shortcode is beign executed and save it
            $postID = get_the_ID();
            $currentURL = get_permalink($postID);
            $saveICPurl = update_option('icp_url', $currentURL);

            //load icp page structure
            if(!is_admin()) {
                $productID = get_query_var( 'id' );
                $siteID = get_query_var( 'site' );
                $blockID = get_query_var( 'block' );
                if(!empty($productID) && !empty($siteID) && !empty($blockID)) {
                    $this->loadProductViewData($productID, $siteID, $blockID, $currentURL);
                } else {
                    echo '<p>'.__('Please select a valid product.','printspot-plugins').'</p>';
                }
            }
        }

        /**
         * laod product view data
         */
        function loadProductViewData($productID, $siteID, $blockID, $currentURL) {            
            $recoverProductDataURL = 'http://test.printspot.io/test-peleman/icp/'. $siteID.'/'.$productID.'/';
            //$recoverProductDataURL = 'http://localhost/printspot/pinxu/icp/'. $siteID.'/'.$productID.'/';
            $productDataJSON = file_get_contents($recoverProductDataURL);
            $productData = json_decode($productDataJSON);
            //view
            include('views/product_view.php');
        }

        /**
         * get block title
         */
        function getBlockTitle($blockID, $productData) {

            //get block title
            foreach($productData->blocks as $block) {
                $blocksData[$block->definition->block_id] = $block->definition;
            }

            echo '<div class="block_title_header">';
                echo '<div class="block_title"><h2>'.$blocksData[$blockID]->block_name.'</h2></div>';
            echo '</div>';
        }

        /**
         * get product blocks
         */
        function getProductBlocksFlow($productData, $productID, $siteID, $blockID, $currentURL, $wproduct) {

            //get product blocks
            foreach($productData->blocks as $block) {
                $blocksData[$block->definition->block_id]['name'] = $block->definition->block_name;
                $blocksData[$block->definition->block_id]['order'] = $block->definition->block_order;
                $blocksData[$block->definition->block_id]['type'] = $block->definition->block_type;
            }

            //if project exists, get project state
            global $wpdb;
            if(isset($_GET['icp_project'])) {
                $projectID = $_GET['icp_project'];
                $productProjectComponentsTable = $wpdb->prefix.'icp_products_projects_components'; 
                $getProjectData = $wpdb->get_row("SELECT value FROM ".$productProjectComponentsTable." WHERE project=$projectID");
                $projectData = unserialize($getProjectData->value);
            }

            //build blocks flow navigator
            if(is_user_logged_in()) {
                $userInfo = wp_get_current_user();
            }

            //get theme color
            if(get_option('shop_primary_color')) {
                $primaryColor = get_option('shop_primary_color');
            } else {
                $primaryColor = get_option('wc_settings_tab_imaxel_icp_color_theme');
            }
            echo '<style>
                    .lds-ring div {border-color: '.$primaryColor.' transparent transparent transparent !important;}
                    .lds-ellipsis div {background: '.$primaryColor.'}
            </style>';

            echo '<div class="icp_header_box">';

                echo '<div class="icp_flow_box_header">';
                    //product title
                    echo '<div class="icp_product_title">';
                        echo '<h1>'.$productData->definition->name.'</h1>';
                    echo '</div>';
                    //user account
                    echo '<div class="icp_save_section">';
                    if(isset($_GET['icp_project'])) {
                        if(is_user_logged_in()) {
                            //save button
                            echo '<p><span project_id="'.$projectID.'" i style="cursor:pointer;" id="save_icp_project" class="save_icp_project">'.__('Save','printspot-plugins').'<i class="far fa-save fa-lg"></i></span><span style="display:none;" class="saving_message">'.__('Saving...','printspot-plugins').'</span>';
                            //user
                            echo '<span class="icp_user">'.__('Hi','printspot-plugins').','.$userInfo->data->display_name.'!<i class="fas fa-user fa-lg"></i></span></p>';
                        } else {
                            echo '<p><a target="__blank" href="'.get_permalink( get_option('woocommerce_myaccount_page_id') ).'"><span style="cursor:pointer;" class="icp_user">'.__('Login to save','printspot-plugins').'<i class="fas fa-user fa-lg"></i></span></a></p>';
                        }
                    }
                    echo '</div>';
                echo '</div>';


                echo '<div class="icp_flow_navigator_box" style="border-bottom-color: '.$primaryColor.'">';
                foreach($blocksData as $block => $flowBlock) {
                    echo '<div class="icp_flow_navigator_items_box">';

                        echo '<div class="icp_flow_navigator_item';

                        if(!isset($_GET['icp_project'])) {
                            if($blockID == $block) {
                                echo '" style="background-color:'.$primaryColor.';';
                            }
                        } else {
                            $projectID = $_GET['icp_project'];
                            if(isset($projectData[$block])) {
                                echo ' block_return" block="'.$block.'" style="background-color:'.$primaryColor.'; cursor: pointer; returnURL="'.$currentURL.'" product_id="'.$productID.'" site_id="'.$siteID.'" block_id="'.$block.'" project_id="'.$projectID.'"';
                                if(!empty($wproduct)) {
                                    echo ' wproduct="'.$wproduct.'"';
                                }
                            } else if($blockID == $block) {
                                echo '" style="background-color:'.$primaryColor.';   opacity: 0.5;';
                            }
                        }
                        
                        echo '">';
                            echo '<p><strong>'.$flowBlock['name'].'</strong></p>';
                        echo '</div>';

                        echo '<div id="lds_ellipsis_'.$block.'" class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>';

                    echo '</div>';
                }
                echo '</div>';

            echo '</div>';
        }
        
        /**
         * get next block for the edition flow
         */
        function getNextBlock($icpURL, $productID, $currentBlock, $siteID, $projectID, $wproduct = '') {

            //get product data
            $recoverProductDataURL = 'http://test.printspot.io/test-peleman/icp/'. $siteID.'/'.$productID.'/';
            //$recoverProductDataURL = 'http://localhost/printspot/pinxu/icp/'. $siteID.'/'.$productID.'/';
            $productDataJSON = file_get_contents($recoverProductDataURL);
            $productData = json_decode($productDataJSON);
            foreach($productData->blocks as $block) {
                $blocksDataByID[$block->definition->block_id] = $block->definition;
                $blockDataByOrder[$block->definition->block_order] = $block->definition;
            }

            $currentBlockOrder = $blocksDataByID[$currentBlock]->block_order;
            $nextBlockOrder = $currentBlockOrder + 1;
            $nextBlockID = $blockDataByOrder[$nextBlockOrder]->block_id;
            $nextBlockType = $blockDataByOrder[$nextBlockOrder]->block_type;

            if($nextBlockType == 'confirmation') {
                //update project
                $currentProjectData = $this->getProjectData($projectID);
                $currentProjectData[$nextBlockID]['confirmation'] = 1;
                $newProjectData = serialize($currentProjectData);

                //Update project components
                $updateProjectComponents = $this->updateProjectComponent($newProjectData, $projectID);
            }

            //build new url for next block
            $newBlockURL = trim(strtok($icpURL, '?')).'?id='.$productID.'&site='.$siteID.'&block='.$nextBlockID.'&icp_project='.$projectID.'';
            if(!empty($wproduct)) { $newBlockURL .= '&wproduct='.$wproduct;}

            return $newBlockURL;
        }

        /**
         * return to edited block
         */
        function returnToBlock() {
            $currentURL = $_POST['returnURL'];
            $projectID = $_POST['icpProject'];
            $siteID = $_POST['siteOrigin'];
            $productID = $_POST['productID'];
            $blockID = $_POST['blockID'];
            $wproduct = $_POST['wproduct'];

            //build new url for next block
            $blockURL = trim(strtok($currentURL, '?')).'?id='.$productID.'&site='.$siteID.'&block='.$blockID.'&icp_project='.$projectID;
            if(!empty($wproduct)) {
                $blockURL .= '&wproduct='.$wproduct;
            }

            echo $blockURL;
            wp_die();
        }

        /**
         * get block attributes
         */
        function getBlockAttributes($siteOrigin, $productID, $blockID, $productData) {

            foreach($productData->blocks as $block) {
                if(isset($block->attributes)) {
                    $blockAttributes[$block->definition->block_id] = $block->attributes;
                }
            }

            foreach($blockAttributes[$blockID] as $attribute) {
                $attributeValues[$attribute->definition->attribute_type]['name'] = $attribute->definition->attribute_name;
                foreach($attribute->values as $values) {
                    $attributeValues[$attribute->definition->attribute_type]['value_key'][] =  $values->value_key;
                    $attributeValues[$attribute->definition->attribute_type]['value_data'][] =  $values->value_data;
                }
            }
            
            return $attributeValues;
        }

        /**
         * get project data
         */
        function getProjectData($projectID) {
            global $wpdb;
            $projectComponentsTable = $wpdb->prefix.'icp_products_projects_components';
            $projectData = $wpdb->get_row("SELECT value FROM ".$projectComponentsTable." WHERE project=$projectID");
            $currentProjectData = unserialize($projectData->value);
            return $currentProjectData;
        }

        /**
         * get variation and attribute data
         */
        function getVariationAttributeData($variation, $siteID, $productID) {
            global $wpdb;
            $variationID = intval($variation);

            //get product data
            $recoverProductDataURL = 'http://test.printspot.io/test-peleman/icp/'. $siteID.'/'.$productID.'/';
            //$recoverProductDataURL = 'http://localhost/printspot/pinxu/icp/'. $siteID.'/'.$productID.'/';
            $productDataJSON = file_get_contents($recoverProductDataURL);
            $productData = json_decode($productDataJSON);

            //reorganize data by product type
            foreach($productData->blocks as $block) {
                $blocksData[$block->definition->block_type] = $block;
            }

            //group variation data
            $variation = $blocksData['product_definition']->variations->$variationID; 
            $attributes = $blocksData['product_definition']->attributes; 

            //generate variation data
            $variationData['image'] = $variation->image;
            $variationData['price'] = $variation->price;
            foreach($attributes as $attribute) {
                $attributeID = $attribute->definition->attribute_id;
                $valueID = $variation->attributes->$attributeID->value;
                $variationData['attributes'][$attribute->definition->attribute_slug]['id'] = $attributeID;
                $variationData['attributes'][$attribute->definition->attribute_slug]['value']['id'] = $variation->attributes->$attributeID->value;
                $variationData['attributes'][$attribute->definition->attribute_slug]['value']['key'] = $attributes->$attributeID->values->$valueID->value_key;
                $variationData['attributes'][$attribute->definition->attribute_slug]['value']['data'] = $attributes->$attributeID->values->$valueID->value_data;
                $variationData['attributes'][$attribute->definition->attribute_slug]['type'] = $attribute->definition->attribute_type;
            }

            return $variationData;
        }

        /**
         * get PDF attrbiutes data
         */
        function getPdfVariationAttributesData($projectData, $siteID, $productID) {
            //order blocks by type
            foreach($projectData as $data) {
                foreach($data as $type => $value) {
                    if($type == 'variation') {
                        $variationID = $value['id'];
                    }
                    //($type == 'variation') ? $projectData[$type] = $value['id'] : $projectData[$type][] = $value['id']; 
                }
            } 

            //get variation data
            //$variationID = $projectData['variation'];
            $variationData = $this->getVariationAttributeData($variationID, $siteID, $productID);

            //get pdf attributes
            foreach($variationData['attributes'] as $name => $attribute) {
                if($attribute['type'] == 'pdf_upload') {
                    $pdfAttributes[$name] = $attribute;
                }
            }

            return $pdfAttributes;
        }

        /**
         * update project component
         */
        function updateProjectComponent($newProjectData, $projectID) {
            global $wpdb;
            $projectComponentsTable = $wpdb->prefix.'icp_products_projects_components';
            $sql = "UPDATE ".$projectComponentsTable." SET value='$newProjectData' WHERE project=$projectID";
            $updateProjectComponents = $wpdb->query($sql);
        }

        /**
         * save project
         */
        function saveICPproject() {
            //check if the user is logged
            if(is_user_logged_in()) {
                global $wpdb;
                $icpProject = $_POST['icpProject'];
                $userID = get_current_user_id();

                //associate project with user id
                $projectsTable = $wpdb->prefix.'icp_products_projects';
                $sql = "
                    UPDATE ".$projectsTable." 
                    SET user = $userID
                    WHERE id= $icpProject
                ";
                $saveProject = $wpdb->query($sql);
                wp_die();
            }
        }

        //============WOOCOMMERCE FUNCTIONS============//
        //=============================================//
        /**
         * Register new endpoints to use inside My Account page.
         */
        function my_account_new_endpoints() {
            add_rewrite_endpoint( 'icp_projects', EP_ROOT | EP_PAGES );
        }

        /**
         * Get new endpoint content
         */
        function icp_projects_endpoint_content() {
            $path = 'includes/woocommerce-templates/myaccount-icp-projects.php';
            include($path);
        }

        /**
         * Edit my account menu order
        */
        function icp_my_account_menu_order() {
            $menuOrder = array(
                'dashboard'             => __( 'Projects', 'printspot-plugins' ),
                'icp_projects'          => __( 'ICP Projects', 'printspot-plugins'),
                'orders'                => __( 'Orders', 'printspot-plugins'),
                'edit-address'          => __( 'Addresses', 'woocommerce' ),
                'edit-account'    		=> __( 'Account details', 'printspot-plugins' ),
                'customer-logout'    	=> __( 'Logout', 'printspot-plugins' ),
            );
            return $menuOrder;
        }

        //==========SPECIFIC BLOCKS FUNCTIONS==========//
        //=============================================//

        /**
         * PRODUCT_DEFINITION: create new icp project
         */
        function createNewIcpProject() {
            global $wpdb;
            $projectsTable = $wpdb->prefix.'icp_products_projects';
            $projectComponentsTable = $wpdb->prefix.'icp_products_projects_components';

            //get data form
            $productForm = $_POST['productform'];
            foreach($productForm as $data) {
                $projectData[$data['name']] = $data['value']; 
            }

            $productID = $projectData['product_id'];
            $productName = $projectData['product_name'];
            $variationID = $projectData['variation_id'];
            $variationPrice = $projectData['variation_price'];
            $blockID = $projectData['block_id'];
            $siteID = $projectData['site_origin'];
            $icpURL = $projectData['icp_url'];
            if(isset($projectData['wproduct'])) {
                $wproduct = $projectData['wproduct'];
            } else {
                $wproduct = -1;
            }

            //get active project
            $activeProject = intval($_POST['activeProject']);

            //get selected variation data
            $selectedVariation = $_POST['selectedvariation'];
            //$recoverProductDataURL = 'http://test.printspot.io/test-peleman/icp/'. $siteID.'/'.$productID.'/';
            //$recoverVariationDataURL = 'http://localhost/printspot/pinxu/var/'. $productID.'/'.$selectedVariation.'/';
            $recoverVariationDataURL = 'http://test.printspot.io/test-peleman/var/'. $productID.'/'.$selectedVariation.'/';
            $variationDataJSON = file_get_contents($recoverVariationDataURL);
            $variationData = json_decode($variationDataJSON);

            //generate values serialized array
            $variation[$blockID]['variation']['id'] = $selectedVariation;
            foreach($variationData as $attribute => $data) {
                $variation[$blockID]['variation']['attributes'][$attribute] = $data;
            }

            $blockValues = serialize($variation);

            //create project or override it if already exists
            if($activeProject == 0) {

                //get user id if logged
                if(!empty(get_current_user_id())) {
                    $userID = get_current_user_id();
                    //create project
                    $sql = "
                    INSERT INTO ".$projectsTable."
                        (product, product_name, variation, variation_price, site, first_block, user, woo_product)
                    VALUES
                        ($productID, '$productName', $variationID, $variationPrice, $siteID, $blockID, $userID, $wproduct)
                    ";
                    $createProject = $wpdb->query($sql);
                } else {
                    //create project
                    $sql = "
                    INSERT INTO ".$projectsTable."
                        (product, product_name, variation, variation_price, site, first_block, user, woo_product)
                    VALUES
                        ($productID, '$productName', $variationID, $variationPrice, $siteID, $blockID,  NULL, $wproduct)
                    ";
                    $createProject = $wpdb->query($sql);
                }

                $activeProject = $wpdb->insert_id;

                //save project components
                $sql = "
                    INSERT INTO ".$projectComponentsTable."
                        (project, block, value)
                    VALUES
                        ($activeProject, $blockID, '$blockValues')
                ";
                $saveProjectComponents = $wpdb->query($sql);

            } else {

                //upadet project variation
                $sql = "
                    UPDATE ".$projectsTable."
                    SET variation = $variationID, variation_price=$variationPrice
                    WHERE id=$activeProject
                ";
                $updateProject = $wpdb->query($sql);

                //update project components
                $sql = "
                    UPDATE ".$projectComponentsTable."
                    SET value = '".$blockValues."'
                    WHERE project=$activeProject
                ";
                $updateProjectComponents = $wpdb->query($sql);
            }

            //stablish session id and associate project with it
            $_SESSION['sessionProject'] = $activeProject;   

            //get next block id
            $nextBlockURL = $this->getNextBlock($icpURL, $productID, $blockID, $siteID, $activeProject, $wproduct);

            echo $nextBlockURL;
  
            wp_die();
        }

        /**
         * PDF_UPLOADER: dislpay pdf thumbnail and continue button
         */
        function displayPDFsummary($pdfResults, $pdfFile, $projectID, $blockID, $siteID, $currentURL, $pdfKey, $productID, $wproduct) {
            //get and set theme color
            if(get_option('shop_primary_color')) {
                $primaryColor = get_option('shop_primary_color');
            } else {
                $primaryColor = get_option('wc_settings_tab_imaxel_icp_color_theme');
            }
            echo '<style>
                    .lds-ring div {border-color: '.$primaryColor.' transparent transparent transparent !important;}
                    .lds-ellipsis div {background: '.$primaryColor.'}
                </style>';
            echo '<div class="pdf_summary_box">';

            //print response
            echo '<p>'.__('Great, your PDF has been correctly validated!','printspot-plugins').'</p>';
            //pdf file
            if(isset($pdfResults['result']->descriptor->pages[0]->thumbnail_512)) {
                //pdf thumbnail
                echo '<div class="pdf_file_box">';

                    echo '<div class="pdf_thumbanil">';
                        echo '<img src="'.$pdfResults['result']->descriptor->pages[0]->thumbnail_512.'">';
                    echo '</div>';
                    //pdf summary
                    echo '<div class="pdf_file_summary">';
                        echo '<p><strong>'.__('File name','printspot-plugins').': </strong>'.$pdfFile['name'].'</p>';
                        echo '<p><strong>'.__('Real Size','printspot-plugins').': </strong>'.$pdfResults['result']->descriptor->pages[0]->size->width.' / '.$pdfResults['result']->descriptor->pages[0]->size->height.'pt</p>';
                        echo '<p><strong>'.__('Pages','printspot-plugins').': </strong>'.$pdfResults['result']->descriptor->numPages.'</p>';
                        echo '<div style="float: left;" class="button-loader-box">';
                        echo '<div class="button" onclick="savePdfBlock()" id="savePdfBlock" pdf_name="'.$pdfFile['name'].'" project_id="'.$projectID.'" block_id="'.$blockID.'" site_id="'.$siteID.'" currentURL="'.$currentURL.'" pdf_id="'.$pdfKey.'" product_id="'.$productID.'"';
                        if(!empty($wproduct)) {echo ' wproduct="'.$wproduct.'"';}
                        echo '>'.__('Continue','printspot-plugins').'</div>';
                        echo '<div  class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>';
                        echo '</div>';
                    echo '</div>';

                echo '</div>';
            }
        }

        /**
         * PDF_UPLOADER: validate pdf
         */
        function validatePdfUploader() {

            //get data
            $projectID = $_POST['currentProject'];
            $blockID = $_POST['currentBlock'];
            $siteID = $_POST['currentSite'];
            $currentURL = $_POST['currentURL'];
            $productID = $_POST['productID'];
            $wproduct = $_POST['wproduct'];
            $pdfFile = $_FILES['pdf_file'];
            //$recoverProductDataURL = 'http://localhost/printspot/pinxu/icp/'. $siteID.'/'.$productID.'/';
            $recoverProductDataURL = 'http://test.printspot.io/test-peleman/icp/'. $siteID.'/'.$productID.'/';
            $productDataJSON = file_get_contents($recoverProductDataURL);
            $productData = json_decode($productDataJSON);

            //send pdf file to Imaxel Service API
            $pdfResults = $this->icpDoPostCreate($pdfFile);
            $pdfKey = $pdfResults['result']->key;
            if($pdfResults['status'] == 200) {

                //number of pages
                $pdfPages = $pdfResults['result']->descriptor->numPages;

                //current project
                $project = $this->getProjectData($projectID);
                foreach($project as $data) {
                    foreach($data as $type => $value) {
                        ($type == 'variation') ? $projectData[$type] = $value : $projectData[$type][] = $value; 
                    }
                } 

                //current variation data
                $variationID = $projectData['variation']['id'];
                $variationData = $this->getVariationAttributeData($variationID, $siteID, $productID);
                
                //get pdf attributes
                foreach($variationData['attributes'] as $name => $attribute) {
                    if($attribute['type'] == 'pdf_upload') {
                        $pdfAttributes[$name] = $attribute;
                    }
                }

                //get current block type
                foreach($productData->blocks as $block) {
                    $blocksData[$block->definition->block_id] = $block->definition; 
                }
                $blockType = $blocksData[$blockID]->block_type;

                //validate pdf pages
                if(isset($pdfAttributes['pages'])) {
                    switch($blockType) {

                        //for pdf_upload blocks
                        case 'pdf_upload':
                            if(intval($pdfAttributes['pages']['value']['data']) === $pdfPages) {
                                $pagesValidation = true;
                            } else {
                                $pagesValidation = false;
                            }
                        break;

                        //for design blocks
                        case 'design':
                            if($pdfPages === 1) {
                                $pagesValidation = true;
                            } else {
                                $pagesValidation = false;
                            }
                        break;
                    }
                }

                //validate pdf size
                if(isset($pdfAttributes['size'])) {
                    //attribute size to validate
                    $pdfAttributeSize = unserialize($pdfAttributes['size']['value']['data']);
                    //check with pdf real data
                    foreach($pdfResults['result']->descriptor->pages as $key => $pages) {
                        $realPdfWidth = intval($pages->size->width);
                        $realPdfHeight = intval($pages->size->height);
                        if((intval($pdfAttributeSize['width']) - $realPdfWidth <= 1) && (intval($pdfAttributeSize['height']) - $realPdfHeight <= 1)) {
                            $pageSizeCheck[$key++] = 'ok';
                        } else {
                            $pageSizeCheck[$key++] = 'dif';
                        }
                    }
                    //if there is a page with a non valid size
                    $arraySearch = array_search('dif', $pageSizeCheck, true);
                    if(array_search('dif', $pageSizeCheck, true) !== false) {
                        $sizeValidation = false;
                    } else {
                        $sizeValidation = true;
                    }
                }

                //echo response
                if(isset($sizeValidation) || isset($pagesValidation)) {

                    if(isset($sizeValidation) && isset($pagesValidation)) {

                        if($sizeValidation === false || $pagesValidation === false) {
                            if($sizeValidation === false && $pagesValidation === false) {
                                echo '<p>'.__('Sorry, but your PDF size and pages seems to be incorrect for the selected product variation.','printspot-plugins').'</p>';
                            } else if($sizeValidation === false) {
                                echo '<p>'.__('Sorry, but your PDF size seems to be incorrect for the selected product variation.','printspot-plugins').'</p>';
                            } else if($pagesValidation === false) {
                                echo '<p>'.__('Sorry, but your PDF pages seems to be incorrect for the selected product variation.','printspot-plugins').'</p>';
                            } 
                        } else {
                            $displayPDfsummary = $this->displayPDFsummary($pdfResults, $pdfFile, $projectID, $blockID, $siteID, $currentURL, $pdfKey, $productID, $wproduct);
                        }

                    } else if(!isset($sizeValidation) && isset($pagesValidation)) {

                        if($pagesValidation === false) {
                            echo '<p>'.__('Sorry, but your PDF pages seems to be incorrect for the selected product variation.','printspot-plugins').'</p>';
                        } else {
                            $displayPDfsummary = $this->displayPDFsummary($pdfResults, $pdfFile, $projectID, $blockID, $siteID, $currentURL, $pdfKey, $productID, $wproduct);
                        } 

                    } else if(isset($sizeValidation) && !isset($pagesValidation)) {

                        if($sizeValidation === false) {
                            echo '<p>'.__('Sorry, but your PDF size seems to be incorrect for the selected product variation.','printspot-plugins').'</p>';
                        } else {
                            $displayPDfsummary = $this->displayPDFsummary($pdfResults, $pdfFile, $projectID, $blockID, $siteID, $currentURL, $pdfKey, $productID, $wproduct);
                        } 
                    }
  
                } else {
                    echo 'Your validation has failed';
                }

            } else {
                echo $pdfResults['result']->message;
            }

            //finish the process
            wp_die();
           
        }

        /**
         * PDF_UPLOADER: save pdf data
         */
        function savePdfUploader() {

            //get data
            $projectID = $_POST['currentProject'];
            $blockID = $_POST['currentBlock'];
            $siteID = $_POST['currentSite'];
            $currentURL = $_POST['currentURL'];
            $productID = $_POST['productID'];
            $pdfID = $_POST['pdfID'];
            $pdfName = $_POST['pdfName'];
            if(!empty($_POST['wproduct'])) {
                $wproduct = $_POST['wproduct'];    
            } else {
                $wproduct = ''; 
            }

            //Update project components
            $currentProjectData = $this->getProjectData($projectID);
            if(isset($currentProjectData[$blockID])) {
                unset($currentProjectData[$blockID]);
            }
            $currentProjectData[$blockID]['pdf']['key'] = $pdfID;
            $currentProjectData[$blockID]['pdf']['name'] = $pdfName;
            $newProjectData = serialize($currentProjectData);
            $updateProjectComponents = $this->updateProjectComponent($newProjectData, $projectID);
 
            //response new block url
            $newURL = $this->getNextBlock($currentURL, $productID, $blockID, $siteID, $projectID, $wproduct);
            echo $newURL;

            //finish
            wp_die();

        }

        /**
         * PDF_UPLOADER: recive pdf values from API
         */
        private function icpDoPostCreate($file) {
            $token = $this->generateToken("pdfs:create");
            if($token!==null){
                return $this->pdfstorageCreateFile($file["tmp_name"], $token);
            } else {
                return null;
            }
        }

        /**
         * PDF_UPLOADER: upload pdf file to imaxel API
         */
        private function pdfstorageCreateFile($path, $token){
            if (function_exists('curl_file_create')) { // php 5.5+
                $cFile = curl_file_create($path);
              } else { // 
                $cFile = '@' . realpath($path);
            }
              $cFile->mime = "application/pdf";

              $post = array(/*'access_token' => $token,*/'file_content'=> $cFile);
              $ch = curl_init();
              curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    "Authorization: Bearer $token"
                ));
              curl_setopt($ch, CURLOPT_URL,"https://services.imaxel.com/apis/pdfstorage/v2/pdfs");
              curl_setopt($ch, CURLOPT_POST,1);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
              curl_setopt($ch,CURLOPT_HEADER, false);
              curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
              $result=curl_exec($ch);
              $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
              curl_close($ch);
              return array("status"=>$httpcode, "result"=>json_decode($result));
        }

        /**
         * PDF_UPLOADER: get authentification form API
         */
        private function generateToken($scope){
            $timeout=5;
            
            $postData = http_build_query(array(
                "grant_type"=>"client_credentials",
                "scope"=>$scope
            ));

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://services.imaxel.com/apis/auth/v1/token");

            //curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_USERPWD,'A38MSvgYjfJwCpHXHqPop7:R80r5mFZcZJ1NAlsHyqNzzoq3bhzjOEL1iJaH7cZ1D');
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch,CURLOPT_HEADER, false);
            curl_setopt($ch,CURLOPT_AUTOREFERER, false);
            curl_setopt($ch,CURLOPT_POST, strlen($postData));
            curl_setopt($ch,CURLOPT_POSTFIELDS, $postData);
            curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, $timeout);

            $output=curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            if($httpcode===200){
                $json = json_decode($output); 
                return $json->access_token;
            } else {
                return null;
            }
        }

        /**
         * PDF_UPLOADER: read pdf file
         */
        private function readPDFfile($pdfKey) {
            $token = $this->generateToken("pdfs:read");
            $curl = curl_init();
            $headers = array(
                'Content-Type: application/json',
                sprintf('Authorization: Bearer %s', $token)
              );
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers );
            curl_setopt($curl, CURLOPT_URL, "https://services.imaxel.com/apis/pdfstorage/v2/pdfs/".$pdfKey);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $resultData = curl_exec($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);

            if($httpcode===200){
                $result = json_decode($resultData); 
                return $result;
            } else {
                return null;
            }
        }

        /**
         * PDF_UPLOADER: display pdf uploader
         */
        function displayPDFuploader($projectData, $blockID, $projectID, $siteID, $currentURL, $productID, $pdfAttributes, $productData, $wproduct = '') {

            //constrains table and uploader form
            /*===================================================================*/
            echo '<div class="pdf_uploader_form">';
                echo '<div class="pdf_constrains_table">';

                    //get current block type
                    foreach($productData->blocks as $block) {
                        $blocksData[$block->definition->block_id] = $block->definition; 
                    }
                    $blockType = $blocksData[$blockID]->block_type;

                    if(isset($pdfAttributes['size']) || isset($pdfAttributes['pages'])) {
                        echo '<p>'.__('Your PDF file must fit the following constrains: ','printspot-plugins').'</p>';
                        //pdf attribute size
                        if(isset($pdfAttributes['size'])) {
                            $pdfAttributeSize = unserialize($pdfAttributes['size']['value']['data']);
                            echo '<p><strong>'.__('Size','printspot-plugins').': </strong>'.$pdfAttributes['size']['value']['key'].'</p>';
                        }

                        //pdf attribute pages
                        if($blockType == 'pdf_upload') {
                            if(isset($pdfAttributes['pages'])) {
                                echo '<p><strong>'.__('Pages','printspot-plugins').': </strong>';
                                echo $pdfAttributes['pages']['value']['data'].'</p>';
                            }
                        } elseif($blockType == 'design') {
                            echo '<p><strong>'.__('Pages','printspot-plugins').': </strong>1</p>';
                        }
                    } else {
                        echo '<p>'.__('Theres no selected constrains for the valdiation.','printspot-plugins').'</p>';
                    }

                echo '</div>';

                ?>

                <form enctype="multipart/form-data" name="pdf_form" id="pdf_form" method="POST">
                    <input type="hidden" name="MAX_FILE_SIZE" value="1000" />
                    <?php echo '<input name="pdf_file" id="pdf_file" project_id="'.$projectID.'" block_id="'.$blockID.'" site_id="'.$siteID.'" currentURL="'.$currentURL.'" product_id="'.$productID.'"';?>
                    <?php if(!empty($wproduct)) { echo ' wproduct='.$wproduct;}?>
                    <?php echo ' type="file" />';?>
                    <script>
                        jQuery(':file').on('change', function() {
                            var file = this.files[0];
                            if(file.size > 10000000) {
                                alert('Too much weight');
                            }
                        })
                    </script>
                </form>

                <?php 
            echo '</div>';

            //pdf summary box
            /*===================================================================*/
                //get and set theme color
                if(get_option('shop_primary_color')) {
                    $primaryColor = get_option('shop_primary_color');
                } else {
                    $primaryColor = get_option('wc_settings_tab_imaxel_icp_color_theme');
                }
                echo '<style>
                        .lds-ring div {border-color: '.$primaryColor.' transparent transparent transparent !important;}
                        .lds-ellipsis div {background: '.$primaryColor.'}
                </style>';

                echo '<div class="pdf_summary_box">';

                if(isset($projectData[$blockID]['pdf'])) {

                    //recover pdf data
                    $pdfID = $projectData[$blockID]['pdf']['key'];
                    $pdfName = $projectData[$blockID]['pdf']['name'];
                    $pdfData = $this->readPDFfile($pdfID);
                    if(!empty($pdfData)) {
                        $recoverPdf = $pdfData; 
                    } else {
                        $recoverPdf = '<p>This PDF no longer exists</p>';
                    }

                    echo '<p>'.__('Your project is currently using this PDF file:', 'printspot-plugins').'</p>'; 
                
                    echo '<div class="pdf_file_box">';

                        //pdf thumbnail
                        echo '<div class="pdf_thumbanil">';
                            echo '<img src="'.$recoverPdf->descriptor->pages[0]->thumbnail_512.'">';
                        echo '</div>';

                        //pdf details
                        echo '<div class="pdf_file_summary">';
                            echo '<p><strong>'.__('File name','printspot-plugins').': </strong>'.$pdfName.'</p>';
                            echo '<p><strong>'.__('Real Size','printspot-plugins').': </strong>'.$recoverPdf->descriptor->pages[0]->size->width.' / '.$recoverPdf->descriptor->pages[0]->size->height.'pt</p>';
                            echo '<p><strong>'.__('Pages','printspot-plugins').': </strong>'.$recoverPdf->descriptor->numPages.'</p>';
                            echo '<div style="float: left;" class="button-loader-box">';
                                echo '<div style="float:left;" class="button" style="margin-left: 2.5px;" onclick="savePdfBlock()" id="savePdfBlock" pdf_name="'.$pdfName.'" project_id="'.$projectID.'" block_id="'.$blockID.'" site_id="'.$siteID.'" currentURL="'.$currentURL.'" pdf_id="'.$pdfID.'" product_id="'.$productID.'"';
                                if(!empty($wproduct)) {echo 'wproduct="'.$wproduct.'"';}
                                echo '>'.__('Continue','printspot-plugins').'</div>';
                                echo '<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>';
                            echo '</div>';    
                        echo '</div>';

                    echo '</div>';

                }

            echo '</div>';
        }

        /**
         * DESIGN: call the editor
         */
        function openEditor() {
            global $wpdb;
            $siteOrigin = $_POST['siteOrigin'];
            $productID = $_POST['productID']; 
            $productCode = $_POST['dealerProductCode'];
            $variations[] = $_POST['variationCode'];
            $dealerID = $_POST['dealerID'];
            $blockID = $_POST['blockID'];
            $icpProject = $_POST['icpProject'];
            $currentURL = $_POST['returnURL'];
            $wproduct = $_POST['wproduct'];

            //get dealer info
            if(is_plugin_active('imaxel-printspot/imaxel-printspot.php')) {
                $sitePrefixTables = $wpdb->get_blog_prefix($siteOrigin);
                $dealersTable = $sitePrefixTables.'imaxel_printspot_shop_dealers';
                $dealerData = $wpdb->get_row("SELECT private_key, public_key FROM ".$dealersTable." WHERE id=".$dealerID."");
                $publicKey = $dealerData->public_key;
                $privateKey = $dealerData->private_key; 
            } else {
                $publicKey = get_option("wc_settings_tab_imaxel_publickey");
                $privateKey = get_option("wc_settings_tab_imaxel_privatekey");
            }

            //build editor url
            $currentURLParameters = "?icp_add_design&id=".$productID."&site=".$siteOrigin."&block=".$blockID."&icp_project=".$icpProject;
            if(!empty($wproduct)) {
                $currentURLParameters .= "&wproduct=".$wproduct;
            }

            //build back url from editor
            $backURL = get_option('icp_url')."?id=".$productID."&site=".$siteOrigin."&block=".$blockID."&icp_project=".$icpProject;
            if(!empty($wproduct)) {
                $backURL .= "&wproduct=".$wproduct;
            }

            //editor call
            $icpOperations = new icpOperations();
            $response = $icpOperations->icpCreateProject($publicKey, $privateKey, $productCode, $variations, $currentURL, $currentURLParameters, $backURL);
            echo $response;
            wp_die();
        }

        /**
         * DESIGN: confirm project design
         */
        function confirmProjectDesign() {
            $siteOrigin = $_POST['siteOrigin'];
            $productID = $_POST['productID']; 
            $dealerID = $_POST['dealerID'];
            $blockID = $_POST['blockID'];
            $projectID = $_POST['icpProject'];
            $attributeProyecto = $_POST['attributeProyecto'];
            $currentURL = $_POST['returnURL'];
            $wproduct = $_POST['wproduct'];

            //current project data 
            $currentProjectData = $this->getProjectData($projectID);
            if(isset($currentProjectData[$blockID])) {
                unset($currentProjectData[$blockID]);
            }
            $currentProjectData[$blockID]['dealer_project'] = $attributeProyecto;
            $newProjectData = serialize($currentProjectData);

            //Update project components
            $updateProjectComponents = $this->updateProjectComponent($newProjectData, $projectID);

            //response new block url
            $newURL = $this->getNextBlock($currentURL, $productID, $blockID, $siteOrigin, $projectID, $wproduct);
            echo $newURL;
            wp_die();
        }

        /**
         * CONFIRMATION: add to cart
         */
        function icpAddItemToCart() {
            global $woocommerce;
            if(!empty($_POST['wproduct'])) {
                $productID = $_POST['wproduct'];
                $variationID = '';    
            } else {
                $productID = 22;
                $variationID = 23;
            }
            $variations = array();
            $cart_item_data['icp_project'] = intval($_POST['icpProject']);
            $cart_item_data['icp_product'] = intval($_POST['wproduct']);
            $addItemToCart = WC()->cart->add_to_cart($productID, 1, $variationID, $variations, $cart_item_data);
            $cartURL = wc_get_cart_url();
            echo $cartURL;
            wp_die();
        }

        /**
         *  CONFIRMATION: override cart item data
         */
        function icp_custom_cart_data($cart) {
            global $wpdb;
            $projectsTable = $wpdb->prefix.'icp_products_projects';
            foreach($cart->cart_contents as $key => $value) {
                if(isset($value['icp_project'])) {
                    $produtcID = $value['icp_product'];
                    $projectID = $value['icp_project'];
                    if(isset($value['icp_product'])) {

                        //get project data
                        $projectData = $wpdb->get_row( "SELECT product_name, variation, variation_price FROM ".$projectsTable." WHERE id=".$projectID."");
                        
                        //override name and price
                        $value['data'] -> set_name($projectData->product_name); 
                        $value['data'] -> set_price(floatval($projectData->variation_price));

                    }
                }
            }
        }

        //================PROCESS ORDER================//
        /**
         * IMAXEL CUSTOM ORDERS: post order
         */
        private function icpDoPostOrder($orderData) {
            $token = $this->generateToken("icorders:send");
            if($token!==null){
                return $this->icordersCreateOrder($orderData, $token);
            } else {
                return null;
            }
        }

        /**
         * IMAXEL CUSTOM ORDERS: create order order
         */
        private function icordersCreateOrder($orderData, $token){
            
            $timeout=5;
        
            $postData = $orderData;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Authorization: Bearer $token",
                "Content-Type:application/json"
            ));
            curl_setopt($ch, CURLOPT_URL,"https://services.imaxel.com/apis/icorders/v1/sent-orders");

            //curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch,CURLOPT_HEADER, false);
            curl_setopt($ch,CURLOPT_AUTOREFERER, false);
            curl_setopt($ch,CURLOPT_POSTFIELDS, $postData);
            curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, $timeout);

            $output=curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            if($httpcode===200){
                $json = json_decode($output); 
                return $json;
            } else {
                return null;
            }
        }

        /**
         * IMAXEL CUSTOM ORDERS: new order
         */
        function newICOorder() {

            global $wpdb;
            $icpProjectsTable = $wpdb->prefix.'icp_products_projects';
            $icpProjectsComponentsTable = $wpdb->prefix.'icp_products_projects_components';

            //get icp products from cart
            $cart = WC()->cart->get_cart();
            foreach($cart as $item) {
                if(isset($item['icp_product'])) {
                    $icpProductsToProcess[$item['icp_product']] = $item;
                }
            }

            //build orderData object
            $orderData = "{
                \"jobs\": [";

            foreach($icpProductsToProcess as $icpProduct) {
                $icpProjects = $icpProduct['icp_project'];
                $getProjectsData = $wpdb->get_row("SELECT * FROM ".$icpProjectsTable." WHERE id=".$icpProjects."");
                $getProjectComponentstData = $wpdb->get_row("SELECT value FROM ".$icpProjectsComponentsTable." WHERE project=".$icpProjects."");
                $projectData = unserialize($getProjectComponentstData->value);

                //get variation data
                foreach($projectData as $attribute) {
                    if(isset($attribute['variation'])) {
                        $projectVariation = $attribute['variation'];
                    }
                }

                $orderData .= "{
                    \"product\": {
                        \"code\": \"".$getProjectsData->product."\",
                        \"name\": { \"default\": \"".$getProjectsData->product_name."\" }
                    },";
                
                $orderData .= "\"form\":[";

                foreach($projectVariation['attributes'] as $attribute => $value) {
                    $orderData .= "{
                        \"code\": \"".$attribute."\",
                        \"name\": {\"default\": \"".$value->name."\"},
                        \"value\": {
                            \"type\": \"code_and_name\",
                            \"code\": \"".$value->value."\",
                            \"name\": {\"default\": \"".$value->value."\"}
                        }
                    },";
                }
                $orderData = substr_replace($orderData ,"", -1);

                $orderData .= "],";
                
                //set blocks projects
                $orderData .= "\"blocks\":[";

                foreach($projectData as $block) {

                    if(isset($block['pdf'])) {
                        $orderData .= "{
                                \"type\": \"pdf\",
                                \"pdf_key\": \"".$block['pdf']['key']."\"
                        },";
                    }

                    if(isset($block['dealer_project'])) {
                        $orderData .= "{
                                \"type\": \"project\",
                                \"project_id\": \"".$block['dealer_project']."\"
                        },";
                    }
                    
                }

                $orderData = substr_replace($orderData ,"", -1);

                $orderData .= "]";

                $orderData .= "}";
            }
            $orderData .= "]
                }";

            $putOrder = $this->icpDoPostOrder($orderData);
        }

        //=============================================//
    }
}

// finally instantiate our plugin class and add it to the set of globals
$GLOBALS['Printspot_Custom_Products'] = new Printspot_Custom_Products();