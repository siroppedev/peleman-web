//INDEPENDENT FUNCTIONS

/**
 * save pdf url
 */
function savePdfBlock() {

    window.onbeforeunload = function() {
        return null;
    }

    jQuery('#savePdfBlock').hide();
    jQuery('.button-loader-box .lds-ellipsis').show();
    var currentProject = jQuery("#savePdfBlock").attr('project_id');
    var currentBlock = jQuery("#savePdfBlock").attr('block_id');
    var currentURL = jQuery("#savePdfBlock").attr('currentURL');
    var currentSite = jQuery("#savePdfBlock").attr('site_id');
    var productID = jQuery("#savePdfBlock").attr('product_id');
    var pdfID = jQuery("#savePdfBlock").attr('pdf_id');
    var pdfName = jQuery("#savePdfBlock").attr('pdf_name');
    var wproduct = jQuery("#savePdfBlock").attr('wproduct');
    jQuery.ajax({
        type:"POST",
        url: ajax_object.url,
        data: {
            action: 'savePdfUploader',
            currentProject: currentProject,
            currentBlock: currentBlock,
            currentURL: currentURL,
            currentSite: currentSite,
            productID: productID,
            pdfID: pdfID,
            pdfName: pdfName,
            wproduct: wproduct
        },
        success:function(response) {
            window.location.assign(response);
        },  
        error: function(){
            console.log('Total fail');
        }
    });
}

/**
 * create project
 */
function createNewIcpProject() {

    window.onbeforeunload = function() {
        return null;
    }

    var selectedvariation = jQuery('#icp-start-customizing').attr('active_variation');
    var activeProject = jQuery('#icp-start-customizing').attr('project_id');
    var productform = jQuery("#variation_attributes_form").serializeArray();
    jQuery('#icp-start-customizing').hide();
    jQuery('.button-loader-box .lds-ellipsis').show();

    jQuery.ajax({
        type:"POST",
        url: ajax_object.url,
        data: {
            action: 'createNewIcpProject',
            productform: productform,
            selectedvariation: selectedvariation,
            activeProject: activeProject
        },
        success:function(response) {
            window.location.assign(response);
            //console.log(response);
        },  
        error: function(){
            console.log('Total fail');
        }
    });
}

//WHEN DOCUMENT READY ACTIONS
jQuery(document).ready(function($) { 

    /*
    * validate pdf file
    */
    jQuery('#pdf_file').on('change', function(e) {

        e.preventDefault();
        jQuery('.pdf_summary_box').html('<div class="lds-ring"><div></div><div></div><div></div><div></div></div>');

        var currentProject = jQuery(this).attr('project_id');
        var currentBlock = jQuery(this).attr('block_id');
        var currentSite= jQuery(this).attr('site_id');
        var currentURL= jQuery(this).attr('currentURL');
        var productID= jQuery(this).attr('product_id');
        var wproduct= jQuery(this).attr('wproduct');

        var file = e.target.files;
        var data = new FormData();
        data.append("action", "validatePdfUploader");
        data.append("currentProject", currentProject);
        data.append("currentBlock", currentBlock);
        data.append("currentSite", currentSite);
        data.append("currentURL", currentURL);
        data.append("productID", productID);
        data.append("wproduct", wproduct);
        $.each(file, function(key, value) {
            data.append("pdf_file", value);
        });

        jQuery.ajax({
            type:"POST",
            url: ajax_object.url,
            dataType: 'text',
            processData: false, // Don't process the files
	        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            cache: false,
            data: data,
            success:function(response) {
                jQuery('.pdf_summary_box').html(response);
            },  
            error: function(){
                console.log('Total fail');
            }
        });

    });

    /**
     * call the editor
     */
    jQuery('.design_item_image').click(function() {

        window.onbeforeunload = function() {
            return null;
        }

        var dealerProductCode = jQuery(this).attr('dealer_product_code');
        var productID = jQuery(this).attr('product_id');
        var variationCode = jQuery(this).attr('variation_code');
        var siteOrigin = jQuery(this).attr('site_origin');
        var dealerID = jQuery(this).attr('dealer_id');
        var blockID = jQuery(this).attr('block_id');
        var icpProject = jQuery(this).attr('icp_project');
        var returnURL = jQuery(this).attr('returnURL');
        var wproduct = jQuery(this).attr('wproduct');
        var elementID = jQuery(this).attr('element_id');

        jQuery(this).hide();
        jQuery('#editor_loader_'+elementID).show();

        jQuery.ajax({
            type:"POST",
            url: ajax_object.url,
            data: {
                action: 'openEditor',
                dealerProductCode: dealerProductCode,
                variationCode: variationCode,
                siteOrigin: siteOrigin,
                productID: productID,
                dealerID: dealerID,
                blockID: blockID,
                icpProject: icpProject,
                returnURL: returnURL,
                wproduct: wproduct
            },
            success:function(response) {
                window.location.assign(response);
            },  
            error: function(){
                console.log('Total fail');
            }
        });
    });

    /**
     * design confirm button
     */
    jQuery('#design_confirm_button').click(function() {

        window.onbeforeunload = function() {
            return null;
        }

        jQuery(this).hide();
        jQuery('.design-button-loader-box .lds-ellipsis').show();

        var productID = jQuery(this).attr('product_id');
        var siteOrigin = jQuery(this).attr('site_origin');
        var dealerID = jQuery(this).attr('dealer_id');
        var blockID = jQuery(this).attr('block_id');
        var icpProject = jQuery(this).attr('icp_project');
        var attributeProyecto = jQuery(this).attr('attribute_proyecto');
        var returnURL = jQuery(this).attr('returnURL');
        var wproduct = jQuery(this).attr('wproduct');

        jQuery.ajax({
            type:"POST",
            url: ajax_object.url,
            data: {
                action: 'confirmProjectDesign',
                siteOrigin: siteOrigin,
                productID: productID,
                dealerID: dealerID,
                blockID: blockID,
                icpProject: icpProject,
                attributeProyecto: attributeProyecto,
                returnURL: returnURL,
                wproduct: wproduct
            },
            success:function(response) {
                window.location.assign(response);
            },  
            error: function(){
                console.log('Total fail');
            }
        });
    });

    /**
     * add to cart
     */
    jQuery('#icp-add-to-cart').click(function() {

        window.onbeforeunload = function() {
            return null;
        }

        jQuery(this).hide();
        jQuery('.button-loader-box .lds-ellipsis').show();
        
        var icpProject = jQuery(this).attr('icp_project');
        var wproduct = jQuery(this).attr('wproduct');

        jQuery.ajax({
            type:"POST",
            url: ajax_object.url,
            data: {
                action: 'icpAddItemToCart',
                icpProject: icpProject,
                wproduct: wproduct
            },
            success:function(response) {
                window.location.assign(response);
            },  
            error: function(){
                console.log('Total fail');
            }
        });
    });

    /**
     * return to design block
     */
    //add hover effect
    jQuery('.block_return').hover(function() {
        jQuery(this).css("opacity","0.5");
    }, function() {
        jQuery(this).css("opacity","1");
    });

    /**
     * triggers function to return
     */
    jQuery('.block_return').click(function() {

        window.onbeforeunload = function() {
            return null;
        }
        
        var productID = jQuery(this).attr('product_id');
        var siteOrigin = jQuery(this).attr('site_id');
        var blockID = jQuery(this).attr('block_id');
        var icpProject = jQuery(this).attr('project_id');
        var returnURL = jQuery(this).attr('returnURL');
        var wproduct = jQuery(this).attr('wproduct');
        jQuery(this).hide();
        jQuery('#lds_ellipsis_'+blockID).show();

        jQuery.ajax({
            type:"POST",
            url: ajax_object.url,
            data: {
                action: 'returnToBlock',
                icpProject: icpProject,
                siteOrigin: siteOrigin,
                productID: productID,
                blockID: blockID,
                returnURL: returnURL,
                wproduct: wproduct
            },
            success:function(response) {
                window.location.assign(response);
            },  
            error: function(){
                console.log('Total fail');
            }
        });

    });

    /**
     * save project
     */
    jQuery('#save_icp_project').click(function() {
        var icpProject = jQuery(this).attr('project_id');
        jQuery('.saving_message').show();
        jQuery('.save_icp_project').hide();
        jQuery.ajax({
            type:"POST",
            url: ajax_object.url,
            data: {
                action: 'saveICPproject',
                icpProject: icpProject
            },
            success:function(response) {
                jQuery('.saving_message').hide();
                jQuery('.save_icp_project').show();
            },  
            error: function(){
                console.log('Total fail');
            }
        });
    });

}) //document ready closes