
<script>
    jQuery(document).ready(function($){

        //warning when leaving page
        window.onbeforeunload = function() {
            return 'Are you sure you want to leave?';
        }

    });
</script>

<div id="app" class="icp-box">

<?php

//check if current session has an active project or user is logged in and the project belongs him
if( (is_user_logged_in()) && (isset($_GET['icp_project'])) ) {
    global $wpdb;
    $userID = get_current_user_id();
    $projectID = $_GET['icp_project'];
    $getCurrentProjectUser = $wpdb->get_row("SELECT user FROM ".$wpdb->prefix."icp_products_projects WHERE id=$projectID");
    (intval($getCurrentProjectUser->user) == intval($userID)) ? $projectBelongsToUser = true: $projectBelongsToUser = false;
}

if( (isset($_GET['icp_project']) && (isset($_SESSION['sessionProject']) && $_SESSION['sessionProject'] == $_GET['icp_project']) ) || !isset($_GET['icp_project']) || ( isset($projectBelongsToUser) && $projectBelongsToUser == true ) ) {

    //get product flow
    $productID = $_GET['id'];
    $siteID = $_GET['site'];
    $blockID = $_GET['block'];
    if(isset($_GET['wproduct'])) {
        $wproduct = $_GET['wproduct'];
    } else {
        $wproduct = '';
    }
    $productBlocks = $this->getProductBlocksFlow($productData, $productID, $siteID, $blockID, $currentURL, $wproduct);

    //load views
    $blogType = $this->getCurrentBlogType($productData);
    echo '<div class="block_content">';
    switch($blogType) {

        //product definition block
        case 'product_definition':
            include('block_views/product_definition.php');
            include('block_models/product_definition_model.php');
        break;

        //pdf uploader block
        case 'pdf_upload':
            include('block_views/pdf_upload.php');
        break;

        //design block
        case 'design':
            include('block_views/design.php');
        break;

        //confirmation block
        case 'confirmation':
            include('block_views/confirmation.php');
        break;
    }
    echo '</div>';

} else {
    echo '<p>'.__('Sorry but your are not allowed to be here!','printspot-plugins').'</p>';
}

?>

</div>