<?php
//get project id if defined
if(isset($_GET['icp_project'])) {
    $projectID = $_GET['icp_project'];
} else {
    $projectID = null;
}

//get theme color
if(get_option('shop_primary_color')) {
    $primaryColor = get_option('shop_primary_color');
} else {
    $primaryColor = get_option('wc_settings_tab_imaxel_icp_color_theme');
}
echo '<style>.lds-ellipsis div {background:'.$primaryColor.';}</style>';

//get block name
$blockTitle = $this->getBlockTitle($blockID, $productData);

?>

<div class="page-loader">
    <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
</div>

<div class="icp-definition-box">

    <div class="icp-definition-box-content">

        <div class="icp-definition-box-left">
            <div class="icp-definition-product-image">
                <img v-if="activeVariation" :src="variations[activeVariation].image">
                <img v-else="activeVariation" :src="productDefinition.image">
            </div>
        </div>

        <div class="icp-definition-box-right">

            <div class="product_definition_form">
                <p style="font-size: 17px;"><strong><?php echo __('Select the attributes of your product:','printspot-plugins') ?></strong></p>
                <form id="variation_attributes_form" name="variation_attributes_form" method="POST" action="">
                    <?php if(isset($_GET['wproduct'])) { ?>
                        <input type="hidden" name="wproduct" id="wproduct" value=<?php echo $_GET['wproduct'];?>>
                    <?php } ?>
                    <input type="hidden" name="icp_url" id="icp_url" value=<?php echo $currentURL;?>>
                    <input type="hidden" name="product_id" id="product_id" :value="productDefinition.id">
                    <input type="hidden" name="product_name" id="product_name" :value="productDefinition.name">
                    <input type="hidden" name="site_origin" id="site_origin" :value="siteOrigin">
                    <input type="hidden" v-if="activeVariation" name="variation_price" id="variation_price" :value="variations[activeVariation].price">
                    <input v-if="activeVariation" type="hidden" name="variation_id" :value="variations[activeVariation].id">
                    <input type="hidden" name="block_id" id="block_id" :value="activeBlock.definition.block_id"> 
                    <select-attribute :attributes="attributes" :selected-attr-value="selectedAttrValue" @update-attr-value="udpateAttrValue"></select-attribute>
                </form>
            </div>

            <h3 style="margin-top: 15px;" v-if="activeVariation">{{variations[activeVariation].price}} <?php echo get_woocommerce_currency_symbol();?></h3>

            <div class="active_varitation_actions">

                <div class="clean_attributes" style="color: <?php echo $primaryColor?>;">
                    <p v-if="activeVariation" id="clean-attributes-button"style="cursor:pointer; margin-top: 15px; text-align: right;"  v-on:click="restartVariation"><?php echo __('Modify', 'printspot-plugins');?><i style="margin-left: 5px;" class="fas fa-redo"></i></p>
                    <p v-else="activeVariation" id="clean-attributes-button"style="cursor:pointer; margin-top: 15px; text-align: right;"  v-on:click="restartVariation"><?php echo __('Clean', 'printspot-plugins');?><i style="margin-left: 5px;" class="fas fa-redo"></i></p>
                </div>

                <div class="button-loader-box">
                    <div v-show="activeVariation" onclick="createNewIcpProject()" class="button" project_id="<?php echo $projectID; ?>" id="icp-start-customizing" :active_variation="activeVariation"><?php echo __('Continue', 'printspot-plugins');?></div>
                    <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
                </div>

            </div>

        </div>

    </div>
    
</div>

<div id="ajax_responses"></div>