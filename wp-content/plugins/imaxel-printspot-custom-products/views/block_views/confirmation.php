<?php

global $wpdb;

//get block name
$blockTitle = $this->getBlockTitle($blockID, $productData);

//get project data
$projectID = $_GET['icp_project'];
$productID = $_GET['id'];
if(isset($_GET['wproduct'])) {
    $wproduct = $_GET['wproduct'];
} else {
    $wproduct = '';
}
$project = $this->getProjectData($projectID);
foreach($project as $data) {
    foreach($data as $type => $value) {
        ($type == 'variation') ? $projectData[$type] = $value : $projectData[$type][] = $value; 
    }
} 

//get variation and attributes data
$variationID = $projectData['variation']['id'];
$siteID = $_GET['site'];
$variationData = $this->getVariationAttributeData($variationID, $siteID, $productID);

//display resume
$icpProject = $_GET['icp_project'];
echo '<div class="confirmation_info_box">';

    echo '<div class="confirmation_info_box_content">';

        //left box: image
        echo '<div class="confirmation_info_content_left">';
            echo '<div class="confirmation_info_image_container">';
                echo '<img src="'.$variationData['image'].'">';
            echo '</div>';
        echo '</div>';

        //right box: data

        echo '<div class="confirmation_info_content_right">';

            echo '<h2>'.$productData->definition->name.'</h2>';

            echo '<div class="confirmation_info_components">';

                //attributes
                echo '<div class="confirmation_summary_block">';
                    echo '<h4>'.__('Attributes','printspot-plugins').'</h4>';
                    foreach($variationData['attributes'] as $attribute => $value) {
                        echo '<p>'.$value['value']['key'].'</p>';
                    }
                echo '</div>';

                //PDF
                echo '<div class="confirmation_summary_block">';
                if(isset($projectData['pdf'])) {
                    echo '<h4>'.__('PDF','printspot-plugins').'</h4>';
                    foreach($projectData['pdf'] as $pdfLink) {
                        echo '<p>'.$pdfLink['name'].'</p>';
                    }
                }
                echo '</div>';

                //projects
                echo '<div class="confirmation_summary_block">';
                if(isset($projectData['dealer_project'])) {
                    echo '<h4>'.__('Projects','printspot-plugins').'</h4>';
                    foreach($projectData['dealer_project'] as $pdfLink) {
                        echo '<p>'.$pdfLink.'</p>';
                    }
                }
                echo '</div>';

            echo '</div>';

            //price
            echo '<div class="confirmation_info_price">';
                echo '<h4>'.$variationData['price'].' '.get_woocommerce_currency_symbol().'</h4>';
            echo '</div>';

            //button
            echo '<div class="button-loader-box">';
                echo '<div id="icp-add-to-cart" icp_project="'.$icpProject.'" wproduct="'.$wproduct.'" class="button">'.__('Add to cart','printspot-plugins').'</div>';
                echo '<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>';
            echo '</div>';

        echo '</div>';

    echo '</div>';

echo '</div>';

?>