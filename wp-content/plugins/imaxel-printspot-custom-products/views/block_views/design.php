<?php
//get project data
$projectID = $_GET['icp_project'];
$blockID = $_GET['block'];
$productID = $_GET['id'];
$siteID = $_GET['site'];
if(isset($_GET['wproduct'])) {
    $wproduct = $_GET['wproduct'];
} else {
    $wproduct = '';
}

//get product data
$dealerID = $productData->definition->provider;

//get current project data
$projectData = $this->getProjectData($projectID);

$pdfAttributes = $this->getPdfVariationAttributesData($projectData, $siteID, $productID); 

//get block name
$blockTitle = $this->getBlockTitle($blockID, $productData);

//block design options
/*==============================================================*/
$blockAttributes = $this->getBlockAttributes($siteID, $productID, $blockID, $productData);
echo '<div class="design_block_container">';

    //get theme color
    if(get_option('shop_primary_color')) {
        $primaryColor = get_option('shop_primary_color');
    } else {
        $primaryColor = get_option('wc_settings_tab_imaxel_icp_color_theme');
    }
    echo '<style>
            .design_block_box i {color: '.$primaryColor.';}
    </style>';
    
    //upload design
    if(isset($blockAttributes['pdf_upload'])) {
        echo '<div class="design_block_box" id="upload_design_box">';
            echo '<a id="design_open_pdf_uploader_link"><div id="design_open_pdf_uploader" class="design_block_action"><i class="fas fa-file-upload fa-2x"></i>';
                echo '<h4>'.__('Upload your design','printspot-plugins').'</h4>';
            echo '</div></a>';
        echo '</div>';
    }

    //create new design
    if(isset($blockAttributes['editor_link'])) {
        echo '<div class="design_block_box" id="create_design_box">';
            echo '<a id="design_open_editor_links_link"><div id="design_open_editor_links" class="design_block_action"><i class="fas fa-edit fa-2x"></i>';
                echo '<h4>'.__('Create new design','printspot-plugins').'</h4>';
            echo '</div></a>';
            
        echo '</div>';
    }

    //contact to contract design 
    if(isset($blockAttributes['custom_field'])) {
        echo '<div class="design_block_box" id="contract_design_box">';
            echo '<a href="'.$blockAttributes['custom_field']['value_data'][0].'"><div id="design_open_editor_links" class="design_block_action"><i class="fas fa-envelope-open fa-2x"></i></div>';
                echo '<h4>'.__('Contact us','printspot-plugins').'</h4>';
        echo '</div></a>';
    }

echo '</div>';

//new design option
/*==============================================================*/
echo '<div id="design_editor_links" class="design_editor_links"';

if(isset($_GET['attribute_proyecto']) || isset($projectData[$blockID]['dealer_project'])) {
    echo ' style="">';
} else {
    echo ' style="display: none;">';
}

    //load already existing design==============================*/
    if(isset($_GET['attribute_proyecto']) || isset($projectData[$blockID]['dealer_project'])) {
        echo '<div class="confirm_project_creation design-button-loader-box ">';
            //new attribute_proyecto and already existing project
            if(isset($_GET['attribute_proyecto']) && isset($projectData[$blockID]['dealer_project'])) {
                $attributeProyecto = $projectData[$blockID]['dealer_project'];
                echo '<p>'.__('Your already have a design for this part of the product!','printsot-plugins').'</p>';
            //new attribute_proyecto but still none existing project
            } else if(isset($_GET['attribute_proyecto']) && !isset($projectData[$blockID]['dealer_project'])) {
                $attributeProyecto = $_GET['attribute_proyecto'];
                echo '<p>'.__('Your design has been correctly loaded!','printsot-plugins').'</p>';
            }
            //still no new attribute_proyecto but already existing project
            else if(!isset($_GET['attribute_proyecto']) && isset($projectData[$blockID]['dealer_project'])) {
                $attributeProyecto = $projectData[$blockID]['dealer_project'];
                echo '<p>'.__('Currently you are using the following design on your project:','printsot-plugins').'</p>';
            }
            echo '<p>'.__('Design ID: ','printsot-plugins').' <strong>'.$attributeProyecto.'</strong></p>';
            echo '<div id="design_confirm_button" class="button" returnURL="'.$currentURL.'" attribute_proyecto="'.$attributeProyecto.'" icp_project="'.$projectID.'" block_id="'.$blockID.'" dealer_id="'.$dealerID.'" product_id="'.$productID.'" site_origin="'.$siteID.'" wproduct="'.$wproduct.'">'.__('Continue','printsot-plugins').'</div>';
            echo '<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>';
        echo '</div>';
    }

    //new design===============================================*/
    echo '<div style="margin-bottom: 5px;" id="new_design_header" class="new_design_header">';
        echo '<h4>'.__('Create new design','printspot-plugins').'</h4>';
        echo '<p style="font-size: 17px;">'.__('Select a template for your component design','printspot-plugins').':</p>';
    echo '</div>';

    echo '<div class="editor_links_container">';
        foreach($blockAttributes['editor_link']['value_data'] as $id => $link) {
            $data = unserialize($link);
            if(isset($data['product_code'])) {
                echo '<div class="design_item">';
                    //link image
                    echo '<div class="design_item_image" element_id="'.$id.'" returnURL="'.$currentURL.'" icp_project="'.$projectID.'" block_id="'.$blockID.'" dealer_id="'.$dealerID.'" product_id="'.$productID.'" dealer_product_code="'.$data['product_code'].'" variation_code="'.$data['variations_code'].'" site_origin="'.$siteID.'" wproduct="'.$wproduct.'">';
                        echo '<img src="'.$data['image'].'">';
                    echo '</div>';
                    echo '<p>'.$blockAttributes['editor_link']['value_key'][$id].'</p>';
                    //loader
                    echo '<div class="editor_loader" id="editor_loader_'.$id.'">';
                        echo '<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>';
                    echo '</div>';
                echo '</div>';
            }
        }
    echo '</div>';
echo '</div>';

//upload pdf option
/*==============================================================*/
echo '<div class="design_pdf_uploader" id="design_pdf_uploader"';
if(isset($projectData[$blockID]['pdf']) && !isset($_GET['attribute_proyecto'])) {
    echo ' style="">';
} else {
    echo ' style="display: none;">';
}
    echo '<h4>'.__('Upload your design','printspot-plugins').'</h4>';
    //display pdf uploader
    $pdfuploader = $this->displayPDFuploader($projectData, $blockID, $projectID, $siteID, $currentURL, $productID, $pdfAttributes, $productData, $wproduct);
echo '</div>';

?>

<script>
//append anchor links
if(jQuery(window).width() <= 770) {
    jQuery('#design_open_pdf_uploader_link').attr("href", "#design_pdf_uploader"); 
    jQuery('#design_open_editor_links_link').attr("href", "#design_editor_links"); 
    jQuery('#design_open_pdf_uploader_link, #design_open_editor_links_link').on("click", function(e){
        e.preventDefault();
            jQuery('html, body').animate({
                scrollTop: jQuery( jQuery.attr(this, 'href') ).offset().top
            }, 500);
        return false;
    });
}

jQuery(window).resize(function() {
    if(window.innerWidth <= 770) {
        jQuery('#design_open_pdf_uploader_link').attr("href", "#design_pdf_uploader"); 
        jQuery('#design_open_editor_links_link').attr("href", "#design_editor_links");  
        jQuery('#design_open_pdf_uploader_link, #design_open_editor_links_link').on("click", function(e){
            e.preventDefault();
                jQuery('html, body').animate({
                    scrollTop: jQuery( jQuery.attr(this, 'href') ).offset().top
                }, 500);
            return false;
        });
    } else {
        jQuery('#design_open_pdf_uploader_link').removeAttr("href");
        jQuery('#design_open_editor_links_link').removeAttr("href");
        jQuery('#design_open_pdf_uploader_link, #design_open_editor_links_link').off();
    }
})

//show pdf uploader
jQuery('#design_open_pdf_uploader').click(function() {
    jQuery('.design_editor_links').hide();
    jQuery('.design_pdf_uploader').show();
    jQuery('.ajax_responses').show();
    jQuery('.confirm_project_creation').hide();
    jQuery('.recover_pdf_data_box').show();
})

//show editor links
jQuery('#design_open_editor_links, #change_design_button').click(function() {
    jQuery('.design_pdf_uploader').hide();
    jQuery('.design_editor_links').show();
    jQuery('.ajax_responses').hide();
    jQuery('.confirm_project_creation').show();
    jQuery('.recover_pdf_data_box').hide();
})
</script>