<?php

//check if theres is an existing project
if(isset($_GET['icp_project'])) {
    global $wpdb;
    $projectID = $_GET['icp_project'];
    $siteID = $_GET['site'];
    $productID = $_GET['id'];
    $projectsTable = $wpdb->prefix.'icp_products_projects';
    $projectVariation = $wpdb->get_row("SELECT variation FROM ".$projectsTable." WHERE id=$projectID");

    //set active variation if project already exists
    $activeVariation = $projectVariation->variation;

    //set preselected attrbiute values id project already excists
    $variationAttrbiuteData = $this->getVariationAttributeData($activeVariation, $siteID, $productID);
    foreach($variationAttrbiuteData['attributes'] as $attribute) {
        $selectedAttrValuesArray[$attribute['id']] = $attribute['value']['id'];
    }
    $selectedAttrValues = json_encode($selectedAttrValuesArray);

    //set available attributes if project is already defined
    $originalProductdata = json_encode($productData);
    foreach($selectedAttrValuesArray as $k => $v) {
        $valueArray[$v] = $productData->blocks[0]->attributes->{"$k"}->values->{"$v"};
        $valueObject = (object)$valueArray;
        $productData->blocks[0]->attributes->{"$k"}->values = $valueObject; 
        $valueArray = null;
    }
    $productDataJSON = json_encode($productData);
} else {
    $activeVariation = 0;
    $selectedAttrValues = 0;
    $originalProductdata = json_encode($productData);
}

?>

<script>

jQuery(document).ready(function($) {
    var activeVariationPHP = <?php echo $activeVariation; ?>;

    //set active variation value if project exists
    if(activeVariationPHP > 1) {
        var activeVariation = activeVariationPHP; 
    } else {
        var activeVariation = null;
    }
    var productDataFull = <?php echo $originalProductdata; ?> 
    var productData = <?php echo $productDataJSON; ?> 
    var selectedAttrValues = <?php echo $selectedAttrValues; ?>;

    //set active variation attrbiutes if project exists
    if(selectedAttrValues !== 0) {
        var selectedAttrValue = selectedAttrValues; 
    } else {
        var selectedAttrValue = {};
    }

    //set completed project data attrbiutes
    var activeBlockFull = productDataFull.blocks.find( block => block.definition.block_type==='product_definition' );
    var blockAttributesFull = activeBlockFull.attributes;

    var activeBlock = productData.blocks.find( block => block.definition.block_type==='product_definition' );
    var blockVariations = activeBlock.variations;
    var blockAttributes = activeBlock.attributes;
    var productDefinition = productData.definition; 
    var blockData = productData.blocks; 
    var siteOrigin = productData.site_origin;
    jQuery('.icp-definition-box').show();
    jQuery('.page-loader').hide();

    /**
     * VUE component for attributes form
     */
    Vue.component('selectAttribute', {
        props: {
            attributes: {
                type: Object
            },
            selectedAttrValue: {
                type: Object,
                default: () => ({})
            },
        },
        methods: {
            getAttrValue() {
                var selectedAttr = [this.selectedAttrValue]
                this.$emit('update-attr-value', selectedAttr)
            }
        },
        template: `
            <div class="selectFormBox">
                <li v-for="(attributeValues, attribute_id) in attributes">
                    <label>{{attributeValues.definition.attribute_name}}</label>
                    <select :name="attribute_id" v-model="selectedAttrValue[attribute_id]" @change="getAttrValue(attribute_id)">
                        <option value="" disabled>(Select an option)</option>
                        <option v-for="(value_name, value_id) in attributeValues.values" :value="value_id">
                            {{value_name.value_key}}
                        </option>
                    </select>
                </li>
            </div>
        `,
    })

    /**
     * VUE instance for product definition block
     */
    var app = new Vue({
        el: '.icp-definition-box',
        data: {
            variations: blockVariations,
            initialAttributes: blockAttributesFull,
            attributes: blockAttributes,
            selectedAttrValue: selectedAttrValue,
            activeVariation: activeVariation,
            productDefinition: productDefinition,
            activeBlock: activeBlock,
            siteOrigin: siteOrigin
        },
        methods: {
            udpateAttrValue(selectedAttr) {
                //set valid attributes
                this.selectedAttrValue = selectedAttr[0];
                var validAttributes = getValidAttributes(this.variations, selectedAttr);
                var currentAttributes = this.initialAttributes
                var refreshAttributes = {};
                Object.values(currentAttributes).forEach(function(attribute) {
                    refreshAttributes[attribute.definition.attribute_id] = {}; 
                    refreshAttributes[attribute.definition.attribute_id]['definition'] = {}; 
                    refreshAttributes[attribute.definition.attribute_id]['values'] = {}; 
                    refreshAttributes[attribute.definition.attribute_id]['definition'] = attribute.definition; 
                    Object.values(attribute.values).forEach(function(value) {
                        if(validAttributes[value.attribute].includes(value.id)) {
                            refreshAttributes[attribute.definition.attribute_id]['values'][value.id] = {}
                            refreshAttributes[attribute.definition.attribute_id]['values'][value.id] = value
                        }
                    })
                })
                this.attributes = refreshAttributes;

                //set active variation
                this.activeVariation = getActiveVariations(this.variations, selectedAttr); 
            },
            restartVariation() {
                this.activeVariation = null;
                this.selectedAttrValue = {};
                this.attributes = this.initialAttributes;
            }
        }
    })

    /**
     * get active variation for selected attributes
     */
    function getActiveVariations(variations, selectedAttributes) {
        var attrArray = {}
        selectedAttributes.forEach(function(attr) {
            for(var key in attr) {
                var attrVal = attr[key]
                attrArray[key] = attrVal
            }
        })

        var varArray = {};

        Object.keys(variations).forEach(function(variation) {
            varArray[variation] = {}
        })
        
        Object.values(variations).forEach(function(variation) {
            Object.values(variation.attributes).forEach(function(attribute){
                varArray[attribute.variation][attribute.attribute] = {}
                    varArray[attribute.variation][attribute.attribute] = attribute.value
            })
        })

        var selectedVarId = Object.keys(varArray).find(varID=>isEqual(varArray[varID], attrArray));

        return selectedVarId;
    }

    /**
     * get valid atributes for the form fields
     */
    function getValidAttributes(blockVariations, selectedAttr) {
        var validVariation = {};
        var varAttributeValues = {};
        var activeAttributesArray = {};
        var variationsAttributeValues = {};

        const selectedAttributes = selectedAttr[0];
        const validVariations = Object.keys(selectedAttributes).reduce( 
                (variations, attrKey)=>variations.filter( variation => !selectedAttributes[attrKey] ||  variation.attributes[attrKey].value === selectedAttributes[attrKey]),
                Object.values(blockVariations)
            );

        Object.values(validVariations).forEach(function(varData) {
            Object.keys(varData.attributes).forEach(function(attr_keys) {
                varAttributeValues[attr_keys] = [];
            })
        })

        Object.values(validVariations).forEach(function(varData) {
            Object.values(varData.attributes).forEach(function(attribute) {
                if(varAttributeValues[attribute.attribute].includes(attribute.value) == false) {
                    varAttributeValues[attribute.attribute].push(attribute.value);
                }
            })
        })

        return varAttributeValues;
    }

    /**
     * compare equal between two arrays
     */
    function isEqual(objA, objB) {
        // Create arrays of property names     
        var aProps = Object.getOwnPropertyNames(objA);     
        var bProps = Object.getOwnPropertyNames(objB);

        // If count of properties is different,     
        // objects are not equivalent     
        if (aProps.length != bProps.length) {         
            return false;     
        }      

        for (var i = 0; i < aProps.length; i++) {         
            
            var propName = aProps[i];          
            // If values of same property are not equal,         
            // objects are not equivalent         
            if (objA[propName] !== objB[propName]) {             
                return false;         
            }     
        }

        // If we made it this far, objects     
        // are considered equivalent     
        return true; 

    }
    
}) //document ready closes
    
</script>

<?php //

