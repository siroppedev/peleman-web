<?php
global $wpdb;
$projectsTable = $wpdb->prefix.'icp_products_projects';

echo '<h3>'.__('ICP Projects','printspot-plugins').'</h3>';
echo '<p>'.__('Here your ICP projects:','printspot-plugins').'</p>';

//get user icp projects
$userID = get_current_user_id();
$userProjects = $wpdb->get_results("SELECT * FROM ".$projectsTable." WHERE user=$userID  ORDER BY id DESC");

//icp url
$icpURL = get_option('icp_url');

//display projects list
echo '<table class="icp_projects_list">';
    echo '<tr>';
        echo '<th><p>'.__('ICP Project','printspot-plugins').'</p></th>';
        echo '<th><p>'.__('Date','printspot-plugins').'</p></th>';
        echo '<th><p>'.__('Order','printspot-plugins').'</p></th>';
        echo '<th><p>'.__('Product','printspot-plugins').'</p></th>';
        echo '<th><p>'.__('Actions','printspot-plugins').'</p></th>';
    echo '</tr>';
    foreach($userProjects as $project) {
        echo '<tr>';
            echo '<td><p>'.$project->id.'</p></td>';
            echo '<td><p>'.$project->date.'</p></td>';
            echo '<td><p></p></td>';
            echo '<td><p>'.$project->product_name.'</p></td>';
            echo '<td><a target="__blank" href="'.$icpURL.'?id='.$project->product.'&site='.$project->site.'&block='.$project->first_block.'&icp_project='.$project->id;
            if(!empty($project->woo_product)) {
                echo '&wproduct='.$project->woo_product;
            }
            echo '"><i class="fas fa-edit"></i></a></td>';
        echo '</tr>';
    }
echo '</table>';
