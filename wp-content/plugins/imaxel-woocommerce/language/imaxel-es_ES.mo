��    .      �      �      �  F   �     D     K     i     �     �  -   �     �     �  
   �     �          "     )     0     @     G     Z     j  (   �  '   �  .   �                         5     ;     G     O     X  
   `  
   k     v     �     �     �     �     �     �     �  	   �  	     
          �  ,  F   �     �          $     <     P  +   c     �     �     �  r   �  V   0	     �	     �	     �	     �	     �	     �	     �	     �	  2   
  3   6
     j
     x
     �
  �   �
     	            	   '     1     :     F     U     k     �  ;   �     �     �     �     �     
  
     
        (   A wordpress plugin to integrate imaxel with woocommerce and wordpress. Action Activate automatic production Advanced configuration All variants Allow guest mode Are you sure you want to delete this project? Cantidad de copias Configuration Create now Desktop only Don't override product price Editar Filter Filter products Imaxel Imaxel WooCommerce Imaxel projects Imaxel reprocess order Introduce Private key supplied by Imaxel Introduce Public key supplied by Imaxel Introduce “URL IwebApi” supplied by Imaxel My projects None Order Override product description Price Private key Product Products Project Project ID Public key Select customer Select one product Select one variant Show create button in shop page Status URL IwebApi Update products Updated User name Woo Order Woo Status http://www.imaxel.com Project-Id-Version: Imaxel WooCommerce
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-10-02 14:11+0000
PO-Revision-Date: 2018-10-02 14:13+0000
Last-Translator: marc <marc@imaxel.com>
Language-Team: Español
Language: es_ES
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/ A wordpress plugin to integrate imaxel with woocommerce and wordpress. Accion Activar producción automática Configuración avanzada Todas las variantes Permitir invitados ¿ Estas seguro de eliminar este proyecto ? Cantidad de copias Configuración Crear ahora Producto no disponible para editar desde tablet o smartphone. Por favor acceda a este producto desde un ordenador. Usar el precio del producto de Woocommerce (no usar el precio del producto del editor) Editar Buscar Filtrar productos Imaxel Imaxel WooCommerce Proyectos Imaxel Imaxel reprocesar pedido introduce clave privada Introduce la clave publica suministrada por Imaxel Introduce “URL IwebApi” suministrada por Imaxel Mis proyectos Ninguno Pedido Usar el nombre del producto del editor en el carrito (desmarcar esta opción para utilizar el nombre del producto de Woocommerce) Precio Clave privada Producto Productos Proyecto ID Proyecto Clave pública Selecciona un cliente Selecciona un producto Seleccione una variante Mostrar botón de crear producto en la página de la tienda Estado URL IwebApi Actualizar productos Actualizado Usuario Pedido Woo Estado Woo http://www.imaxel.com 