//================================================MARC EDIT PROJECT FROM THE CART===================================================//
jQuery(document).on("click", "#edit_from_cart", function(event){
    event.preventDefault();
    var projectID = jQuery(this).attr('project_id');
    var cart_item_id = jQuery(this).attr('cart_item_key');
    var backURL=ajax_object.backurl;
    jQuery.ajax({
        url : ajax_object.url,
        type : 'POST',
        datatype: 'json',
        data : {
           action: 'edit_project_form_cart_function',
           projectID : projectID,
           cart_item_id : cart_item_id,
           backURL : backURL
        },
        success: function(response) {
               console.log(response);
               window.location.replace(response);
         },
         error: function(response) {
	     	console.log(response);
	     }
     })
});
//===================================================================================================================================//

jQuery(document).ready( function() {
    if(jQuery(".variations_form").length>0) {
        jQuery(".variations select[name=attribute_proyecto]").parent().parent().hide();

        jQuery(".variations select[name=attribute_proyecto] option:eq(1)").attr("selected", "selected");

        jQuery(".variations select").change(function () {
            jQuery(".variations select[name=attribute_proyecto] option:eq(1)").attr("selected", "selected");
        });

        jQuery(".variations_form").on("woocommerce_variation_select_change", function () {
            jQuery(".crear_ahora_wrapper").hide();
        });

        jQuery(".single_variation_wrap").on("show_variation", function (event, variation) {
            jQuery(".crear_ahora_wrapper").show();
        });

        jQuery(".crear_ahora_wrapper").hide();

        jQuery(".woocommerce-variation-add-to-cart").hide();
    }


    jQuery("a.editor_imaxel").click( function() {

      var productID = jQuery(this).attr("data-productid");

      jQuery(this).hide();
      jQuery("#imx-loader-" + productID).show();

      var arraySelectedVariations ={};

      jQuery('.variations select').each(function() {
          arraySelectedVariations[ jQuery(this).attr("name")] = jQuery(this).val();
      });

      var backURL=ajax_object.backurl;
      jQuery.ajax({
         url : ajax_object.url,
         type : 'POST',
         datatype: 'json',
         data : {
            action: 'imaxel_wrapper',
            productID:productID,
            selectedVariation:JSON.stringify(arraySelectedVariations),
            backURL:backURL
	     },
         success: function(imaxelresponse,myAjax) {
            if(myAjax == "success") {
               console.log(imaxelresponse);
               window.location.replace(imaxelresponse);
            }
            else {
               console.log(imaxelresponse);
               window.location.replace(imaxelresponse);
            }
         },
         error: function(imaxelresponse,myAjax) {
	     	console.log(imaxelresponse);
	     }
      })   
	  return false;
   })
	
});